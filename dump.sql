CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created` varchar(25) NOT NULL,
  `modified` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `type` varchar(10) NOT NULL,
  `author` varchar(50) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `minify` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `libraries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `crossorigin` text NOT NULL,
  `integrity` text NOT NULL,
  `type` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `libraries` (`id`, `name`, `url`, `crossorigin`, `integrity`, `type`, `status`, `position`) VALUES
(1, 'Bootstrap 4.5.2', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css', 'anonymous', 'sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z', 'css', 1, 0);

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `link` varchar(50) NOT NULL,
  `modified` varchar(25) DEFAULT NULL,
  `created` varchar(25) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `robots` varchar(20) DEFAULT 'index, follow',
  `author` varchar(50) NOT NULL,
  `content` longtext DEFAULT NULL,
  `header` tinyint(1) DEFAULT 1,
  `footer` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`id`, `name`, `status`, `link`, `modified`, `created`, `description`, `keywords`, `robots`, `author`, `content`, `header`, `footer`) VALUES
(1, 'Homepage', 1, 'homepage', '2020-08-04 14:48:57', '2020-04-08 15:39:29', 'Home page description', 'homepage, home', 'index, follow', 'admin', '<h1>\n    This is the homepage\n</h1>\n<p class=\"lead\">\n    Simple and beautiful barebone CMS to ease your work. Construct websites with love!\n</p>', 1, 1),
(2, 'Contact', 1, 'contact', '2020-08-04 14:48:51', '2020-07-31 18:00:49', 'Contact page description', 'contactpage, contact', 'index, follow', 'admin', '<h1>\n    This is the contact page\n</h1>', 1, 1),
(3, 'About', 1, 'about', '2020-08-04 14:48:44', '2020-07-31 18:00:53', 'About page description', 'aboutpage, about', 'index, follow', 'admin', '<h1>\n    This is the about page\n</h1>', 1, 1);

CREATE TABLE `password_reset` (
  `id` int(15) NOT NULL,
  `email` text NOT NULL,
  `selector` text NOT NULL,
  `token` text NOT NULL,
  `expires` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `settings` (
  `id` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `robots` varchar(20) NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `default_page_id` varchar(255) NOT NULL,
  `ga_code` varchar(255) NOT NULL,
  `maintenance` tinyint(1) NOT NULL,
  `bundle_all_files` tinyint(1) NOT NULL,
  `header` tinyint(1) NOT NULL,
  `footer` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`id`, `title`, `description`, `keywords`, `author`, `robots`, `favicon`, `default_page_id`, `ga_code`, `maintenance`, `bundle_all_files`, `header`, `footer`) VALUES
(1, 'Demo Website Title', 'Demo Website Description', 'keyword1, keyword2', 'Website Author', 'noindex, nofollow', '', '1', '', 0, 0, 1, 1);

CREATE TABLE `template_components` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `template_components` (`id`, `name`, `content`) VALUES
(1, 'header', '<div class=\"d-flex p-3 flex-column\" style=\"height: 100vh; background-image: linear-gradient(to top, #d5d4d0 0%, #d5d4d0 1%, #eeeeec 31%, #efeeec 75%, #e9e9e7 100%);\">\n    <header class=\"d-flex flex-column justify-content-center\">\n        <h3 class=\"mb-4 text-center\">{{WEBSITE_TITLE}}</h3>\n        <nav class=\"nav justify-content-center\">\n            <a class=\"nav-link\" href=\"\">Home</a>\n            <a class=\"nav-link\" href=\"about\">About</a>\n            <a class=\"nav-link\" href=\"contact\">Contact</a>\n            <a class=\"nav-link\" href=\"invalidlink\">Invalid Link</a>\n            <a class=\"nav-link\" href=\"cms-admin\" target=\"_blank\">cPanel</a>\n        </nav>\n    </header>\n    <div class=\"d-flex flex-column align-items-center justify-content-center\" style=\"flex: 1\">'),
(2, 'footer', '    </div>\n    <footer class=\"d-flex justify-content-center\">\n        <p>Cover template for <a href=\"#\">{{WEBSITE_TITLE}}</a>.</p>\n    </footer>\n</div>');

CREATE TABLE `template_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `robots` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `template_pages` (`id`, `name`, `content`, `description`, `keywords`, `robots`) VALUES
(1, 'not_found', '<h1>Page not found</h1>', 'Page was not found', '', 'noindex, nofollow'),
(2, 'maintenance', '<h1>Website is under construction!</h1>', 'Website is under construction', '', 'noindex, nofollow');

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `fullname` varchar(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `last_activity` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `fullname`, `username`, `password`, `email`, `type`, `last_activity`) VALUES
(1, 'Administrator', 'admin', '$2y$10$cHZZfA031GBHoY4PBLTR3uWp9HFHEtTXk3TJ4JG1ETXDO1dG4U/UK', '', 2, NULL);

ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX_ID` (`id`),
  ADD KEY `INDEX_TYPE` (`type`),
  ADD KEY `INDEX_STATUS` (`status`);

ALTER TABLE `libraries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX_ID` (`id`),
  ADD KEY `INDEX_TYPE` (`type`),
  ADD KEY `INDEX_STATUS` (`status`);

ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX_ID` (`id`),
  ADD KEY `INDEX_LINK` (`link`),
  ADD KEY `INDEX_STATUS` (`status`);

ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX` (`id`);

ALTER TABLE `template_components`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX` (`name`);

ALTER TABLE `template_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX` (`name`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEX_ID` (`id`),
  ADD KEY `INDEX_USERNAME` (`username`),
  ADD KEY `INDEX_EMAIL` (`email`);

ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `libraries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `password_reset`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT;

ALTER TABLE `template_components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;