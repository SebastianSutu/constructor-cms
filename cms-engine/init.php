<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'database.php';
require_once 'globals.php';
require_once 'libraries/index.php';
require_once 'functions/index.php';
require_once 'queries/index.php';
require_once 'routes_checker.php';
