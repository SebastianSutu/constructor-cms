<?php

// date + time now
$current_date_time = date("Y-m-d H:i:s");

// fetch website settings and info
$website_result = $conn->query("SELECT settings.*, pages.link AS `default_page_url` FROM `settings` JOIN `pages` ON settings.default_page_id = pages.id WHERE settings.id = 1");
$website = $website_result->fetch_object();
$website_result->close();

$link_params = isset($_GET['page']) ? explode('/', $_GET['page']) : null;
$link_params = $link_params ? array_filter($link_params) : null;

define("CPANEL_ROUTE", $link_params && count($link_params) >= 1 ? $link_params[0] === 'cms-admin' : null);
define("CLIENT_PAGE", isset($_GET['page']) ? $_GET['page'] : null);
define("ADMIN_PAGE", $link_params && count($link_params) > 1 ? $link_params[1] : null);
define("ADMIN_PAGE_PARAM", $link_params && count($link_params) > 2 ? $link_params[2] : null);
define("USER_ID", isset($_SESSION['id']) ? $_SESSION['id'] : null);
define("LOGGED_IN", isset($_SESSION['id']) ? true : false);

define('HTTP_HTTPS', isset($_SERVER['HTTPS']) ? 'https://'  : 'http://');
define('ROOT_URL', HTTP_HTTPS . $_SERVER['SERVER_NAME'] . str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));

define("WEBSITE_TITLE", $website->title);
define("WEBSITE_DESCRIPTION", $website->description);
define("WEBSITE_KEYWORDS", $website->keywords);
define("WEBSITE_AUTHOR", $website->author);
define("WEBSITE_ROBOTS", $website->robots);
define("WEBSITE_FAVICON", $website->favicon);
define("WEBSITE_MAINTENANCE", $website->maintenance);
define("WEBSITE_DEFAULT_PAGE_ID", $website->default_page_id);
define("WEBSITE_DEFAULT_PAGE_URL", $website->default_page_url);
define("WEBSITE_GA_CODE", $website->ga_code);
define("WEBSITE_BUNDLE_ALL_FILES", $website->bundle_all_files);
define("WEBSITE_HEADER", $website->header);
define("WEBSITE_FOOTER", $website->footer);

// get user info if logged in
if (LOGGED_IN) {
    $user_info_result = $conn->query("SELECT * FROM `users` WHERE `id` = " . USER_ID);
    $user_info = $user_info_result->fetch_object();
    $user_info_result->close();

    define("USER_FULLNAME", $user_info->fullname);
    define("USER_USERNAME", $user_info->username);
    define("USER_EMAIL", $user_info->email);
    define("IS_ADMIN", $user_info->type == 2);
}

if (!CPANEL_ROUTE) {
    // fetch current page info
    $page_url = empty(CLIENT_PAGE) ? WEBSITE_DEFAULT_PAGE_URL : CLIENT_PAGE;
    $page_url = preg_replace('/[^\w_\s\-\/]+/', '', $page_url);
    $current_page_result = $conn->query("SELECT * FROM `pages` WHERE `link` = '$page_url'");
    $current_page = $current_page_result->fetch_object();

    define("PAGE_ID", isset($current_page->id) ? $current_page->id : null);
    define("PAGE_NAME", isset($current_page->name) ? $current_page->name : null);
    define("PAGE_PUBLISHED", isset($current_page->status) ? $current_page->status : null);
    define("PAGE_URL", isset($current_page->link) ? $current_page->link : null);
    define("PAGE_LAST_UPDATE", isset($current_page->modified) ? $current_page->modified : null);
    define("PAGE_CREATED", isset($current_page->created) ? $current_page->created : null);
    define("PAGE_DESCRIPTION", isset($current_page->description) ? $current_page->description : null);
    define("PAGE_KEYWORDS", isset($current_page->keywords) ? $current_page->keywords : null);
    define("PAGE_ROBOTS", isset($current_page->robots) ? $current_page->robots : null);
    define("PAGE_CONTENT", isset($current_page->content) ? $current_page->content : null);
    define("PAGE_HEADER", isset($current_page->header) ? $current_page->header : null);
    define("PAGE_FOOTER", isset($current_page->footer) ? $current_page->footer : null);

    $current_page_result->close();
}
define("PAGE_EXISTS", isset($current_page->id) ? true : false);
