<?php

use MatthiasMullie\Minify;

function minify_CSS($data)
{
    $minified_CSS = new Minify\CSS($data);

    return $minified_CSS->minify();
}

function minify_JS($data)
{
    $minified_JS = new Minify\JS($data);

    return $minified_JS->minify();
}

function build_files_minified_bundle()
{
    global $conn;

    $minified_CSS = new Minify\CSS();
    $minified_JS  = new Minify\JS();

    $files = $conn->query("SELECT `name`, `type` FROM `files` WHERE `status` = 1");
    if ($files->num_rows > 0) {
        while ($file = $files->fetch_object()) {
            ($file->type == 'css' ? $minified_CSS : $minified_JS)->add("assets/$file->type/$file->name.$file->type");
        }
    }

    $minified_CSS->minify("assets/css/css-bundle.minified.css");
    $minified_JS->minify("assets/js/js-bundle.minified.js");
}
