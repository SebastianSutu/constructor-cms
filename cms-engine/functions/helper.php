<?php

function is_admin_page_param_valid($param, $table)
{
    global $conn;

    $param = sanatize($param);

    if (!is_numeric($param)) return false;

    $sql = "SELECT `id` FROM `$table` WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $param);

    if ($stmt->execute()) {
        $stmt->store_result();
        return $stmt->num_rows > 0;
    } else {
        set_notification('error', "There was a database error");
        reload();
    }
}

function compute_short_codes($string)
{
    $shortcodes = [
        "{{WEBSITE_TITLE}}",
        "{{WEBSITE_DESCRIPTION}}",
        "{{WEBSITE_KEYWORDS}}",
        "{{WEBSITE_AUTHOR}}",
        "{{WEBSITE_ROBOTS}}",
        "{{WEBSITE_FAVICON}}",
        "{{WEBSITE_MAINTENANCE}}",
        "{{WEBSITE_DEFAULT_PAGE_URL}}",
        "{{WEBSITE_GA_CODE}}",
    ];
    $computed_shortcodes = [
        WEBSITE_TITLE,
        WEBSITE_DESCRIPTION,
        WEBSITE_KEYWORDS,
        WEBSITE_AUTHOR,
        WEBSITE_ROBOTS,
        WEBSITE_FAVICON,
        WEBSITE_MAINTENANCE,
        WEBSITE_DEFAULT_PAGE_URL,
        WEBSITE_GA_CODE,
    ];

    return str_replace($shortcodes, $computed_shortcodes, $string);
}

function set_notification($type, $message)
{
    $_SESSION[$type] = $message;
}

function format_date_time($date)
{
    $date = date_create($date);
    return date_format($date, "j M, Y, g:i A");
}

function format_date($date)
{
    if ($date) {
        $date = date_create($date);
        return date_format($date, "j M, Y");
    }
}

function is_user_allowed()
{
    global $conn;

    $user_result = $conn->query("SELECT `type` FROM `users` WHERE `id` =" . USER_ID);
    $user = $user_result->fetch_object();
    $user_result->close();

    if ($user->type == 1) {
        set_notification('warning', "You're not allowed to do this!");
        reload();
    }
}

function filter_file_name($name)
{
    $name = strtolower($name);
    $name = preg_replace("/[^a-z\s-]/", "", $name);
    $name = preg_replace("/[\s-]+/", " ", $name);
    $name = preg_replace("/[\s_]/", "-", $name);
    $name = trim($name, "-/");
    return $name;
}

function filter_page_title($title)
{
    $title = preg_replace('/[^a-z0-9]+/i', ' ', $title);
    $title = preg_replace("/[\s-]+/", " ", $title);
    return $title;
}

function filter_page_url($url)
{
    $url = strtolower($url);

    // replace duplicates of "/" with only one
    $url = preg_replace("/\/+/", "/", $url);
    // replace duplicates of space and "-" with only one
    $url = preg_replace("/[\s-]+/", " ", $url);
    $url = preg_replace("/\//", "/", $url);
    $url = preg_replace("/[\s_]/", "-", $url);
    // remove special characters except "/"
    $url = preg_replace('/[^\w_\s\-\/]+/', '', $url);

    $url = str_replace("-/-", "/", $url);
    $url = str_replace("-/", "/", $url);
    $url = str_replace("/-", "/", $url);

    $url = trim($url, "-/");
    return $url;
}

function sanatize($string)
{
    global $conn;
    return trim(filter_var($conn->real_escape_string($string), FILTER_SANITIZE_STRING));
}

function sanatize_email($email)
{
    global $conn;
    return trim(filter_var($conn->real_escape_string($email), FILTER_VALIDATE_EMAIL));
}

function redirect($location, $client = false)
{
    if ($client) {
        header("location: " . ROOT_URL . $location);
    } else {
        header("location: " . ROOT_URL . "cms-admin/"  . $location);
    }
    exit;
}

function reload()
{
    header('Location: ' . $_SERVER['REQUEST_URI']);
    exit;
}

function logout()
{
    session_destroy();
    redirect('login');
}
