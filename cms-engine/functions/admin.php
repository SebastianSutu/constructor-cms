<?php

function notification_handler()
{
    $type_of = ['error', 'warning', 'success'];

    $notifications = "<div class='notification_body fadeInDown animated'>";

    foreach ($type_of as $type) {
        if (isset($_SESSION[$type])) {
            if (!is_array($_SESSION[$type])) $_SESSION[$type] = array($_SESSION[$type]);

            foreach ($_SESSION[$type] as $message) {
                $notifications .= '
                    <div class="notification notification-' . $type . '" data-auto-dismiss>
                       <div class="notification_content">
                           <p class="notification_type">' . ucfirst($type) . '</p>
                           <p class="notification_message">' . $message . '</p>
                       </div>
                       <div class="notification_close">
                           <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.642 15.642" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 15.642 15.642">
                               <path fill-rule="evenodd" d="M8.882,7.821l6.541-6.541c0.293-0.293,0.293-0.768,0-1.061  c-0.293-0.293-0.768-0.293-1.061,0L7.821,6.76L1.28,0.22c-0.293-0.293-0.768-0.293-1.061,0c-0.293,0.293-0.293,0.768,0,1.061  l6.541,6.541L0.22,14.362c-0.293,0.293-0.293,0.768,0,1.061c0.147,0.146,0.338,0.22,0.53,0.22s0.384-0.073,0.53-0.22l6.541-6.541  l6.541,6.541c0.147,0.146,0.338,0.22,0.53,0.22c0.192,0,0.384-0.073,0.53-0.22c0.293-0.293,0.293-0.768,0-1.061L8.882,7.821z"></path>
                           </svg>
                       </div>
                    </div>
                ';
            }

            unset($_SESSION[$type]);
        }
    }

    $notifications .= "</div>";

    return $notifications;
}

function get_base_href()
{
    $url = ROOT_URL  . "cms-admin/";

    return "<base href='$url'>";
}

function get_title()
{
    switch (ADMIN_PAGE) {
        case 'dashboard':
            $title = "Dashboard";
            break;
        case 'statistics':
            $title = "Statistics";
            break;
        case 'pages':
            if (ADMIN_PAGE_PARAM) {
                if (ADMIN_PAGE_PARAM === 'maintenance') {
                    $title = "Edit Maintenance Page";
                } elseif (ADMIN_PAGE_PARAM === 'not-found') {
                    $title = "Edit Not Found Page";
                } else {
                    $title = "Edit Page";
                }
            } else {
                $title = "Pages";
            }
            break;
        case 'files':
            if (ADMIN_PAGE_PARAM) {
                $title = "Edit File";
            } else {
                $title = "CSS & JS Files";
            }
            break;
        case 'media-files':
            $title = "Media Files";
            break;
        case 'header':
            $title = "Edit Header";
            break;
        case 'footer':
            $title = "Edit Footer";
            break;
        case 'libraries':
            if (ADMIN_PAGE_PARAM) {
                $title = "Edit Library";
            } else {
                $title = "CSS & JS Libraries";
            }
            break;
        case 'shortcodes':
            $title = "Shortcodes";
            break;
        case 'settings':
            $title = "Website Settings";
            break;
        case 'users':
            if (ADMIN_PAGE_PARAM) {
                $title = "Edit User Profile";
            } else {
                $title = "Users";
            }
            break;
        case 'profile':
            $title = "Your Profile";
            break;
        case '404':
            $title = "Page not found";
            break;

        case 'login':
            $title = "Login";
            break;
        case 'password-recovery':
            $title = "Password Recovery";
            break;
        case 'create-new-password':
            $title = "Reset Password";
            break;

        default:
            break;
    }

    $title .= " - " . WEBSITE_TITLE;

    return "<title>$title</title>";
}
