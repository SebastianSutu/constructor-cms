<?php

function get_client_base_href()
{
    return ROOT_URL;
}

function get_client_ga_code()
{
    if (!empty(WEBSITE_GA_CODE)) {
?>
        <!-- Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?= WEBSITE_GA_CODE ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', '<?= WEBSITE_GA_CODE ?>');
        </script>
<?php
    }
}

function get_client_css_libraries()
{
    global $conn;

    $libraries = null;

    $css_libraries_result = $conn->query("SELECT `url` FROM `libraries` WHERE `type` = 'css' AND `status` = 1 ORDER BY `position`");
    if ($css_libraries_result->num_rows > 0) {
        while ($css_library = $css_libraries_result->fetch_object()) {
            $libraries .= "<link rel='stylesheet' href='$css_library->url'>" . "\r\n";
        }
    }

    return $libraries;
}

function get_client_js_libraries()
{
    global $conn;

    $libraries = null;

    $js_libraries_result = $conn->query("SELECT `url` FROM `libraries` WHERE `type` = 'js' AND `status` = 1 ORDER BY `position`");
    if ($js_libraries_result->num_rows > 0) {
        while ($js_library = $js_libraries_result->fetch_object()) {
            $libraries .= "<script src='$js_library->url'></script>" . "\r\n";
        }
    }

    return $libraries;
}

function get_client_css_files()
{
    if (WEBSITE_BUNDLE_ALL_FILES) {
        if (empty(file_get_contents('assets/css/css-bundle.minified.css'))) return;

        return "<link rel='stylesheet' href='assets/css/css-bundle.minified.css'>";
    }

    global $conn;

    $css = null;

    $css_result = $conn->query("SELECT `name`, `minify` FROM `files` WHERE `type` = 'css' AND `status` = 1 ORDER BY `position`");
    if ($css_result->num_rows > 0) {
        while ($included_css = $css_result->fetch_object()) {
            if ($included_css->minify) {
                $css .= "<link rel='stylesheet' href='assets/css/$included_css->name.minified.css'>" . "\r\n";
            } else {
                $css .= "<link rel='stylesheet' href='assets/css/$included_css->name.css'>" . "\r\n";
            }
        }
    }

    return $css;
}

function get_client_js_files()
{
    if (WEBSITE_BUNDLE_ALL_FILES) {
        if (empty(file_get_contents('assets/js/js-bundle.minified.js'))) return;

        return "<script src='assets/js/js-bundle.minified.js'></script>";
    }

    global $conn;

    $js = null;

    $js_result = $conn->query("SELECT `name`, `minify` FROM `files` WHERE `type` = 'js' AND `status` = 1 ORDER BY `position`");
    if ($js_result->num_rows > 0) {
        while ($included_js = $js_result->fetch_object()) {
            if ($included_js->minify) {
                $js .= "<script src='assets/js/$included_js->name.minified.js'></script>" . "\r\n";
            } else {
                $js .= "<script src='assets/js/$included_js->name.js'></script>" . "\r\n";
            }
        }
    }

    return $js;
}

function get_client_title()
{
    if (LOGGED_IN) {
        if (!CLIENT_PAGE) {
            $title = WEBSITE_TITLE;
        } elseif (!PAGE_EXISTS || CLIENT_PAGE === WEBSITE_DEFAULT_PAGE_URL) {
            $title = "Page not found - " . WEBSITE_TITLE;
        } else {
            $title = PAGE_NAME . " - " . WEBSITE_TITLE;
        }
    } else {
        if (WEBSITE_MAINTENANCE) {
            $title = "We will be back soon! - " . WEBSITE_TITLE;
        } elseif (!CLIENT_PAGE) {
            $title = WEBSITE_TITLE;
        } elseif (!PAGE_PUBLISHED || !PAGE_EXISTS || CLIENT_PAGE === WEBSITE_DEFAULT_PAGE_URL) {
            $title = "Page not found - " . WEBSITE_TITLE;
        } else {
            $title = PAGE_NAME . " - " . WEBSITE_TITLE;
        }
    }

    return $title;
}

function get_client_description()
{
    if (LOGGED_IN) {
        if (!CLIENT_PAGE || !PAGE_EXISTS) {
            if (!empty(WEBSITE_DESCRIPTION)) {
                return WEBSITE_DESCRIPTION;
            }
        } else {
            if (!empty(PAGE_DESCRIPTION)) {
                return PAGE_DESCRIPTION;
            } else {
                return WEBSITE_DESCRIPTION;
            }
        }
    } else {
        if (!CLIENT_PAGE || WEBSITE_MAINTENANCE || !PAGE_EXISTS || !PAGE_PUBLISHED) {
            if (!empty(WEBSITE_DESCRIPTION)) {
                return WEBSITE_DESCRIPTION;
            }
        } else {
            if (!empty(PAGE_DESCRIPTION)) {
                return PAGE_DESCRIPTION;
            } else {
                return WEBSITE_DESCRIPTION;
            }
        }
    }
}

function get_client_author()
{
    if (!empty(WEBSITE_AUTHOR)) {
        return WEBSITE_AUTHOR;
    }
}

function get_client_keywords()
{
    if (LOGGED_IN) {
        if (!CLIENT_PAGE || !PAGE_EXISTS) {
            if (!empty(WEBSITE_KEYWORDS)) {
                return WEBSITE_KEYWORDS;
            }
        } else {
            if (!empty(PAGE_KEYWORDS)) {
                return PAGE_KEYWORDS;
            } else {
                return WEBSITE_KEYWORDS;
            }
        }
    } else {
        if (!CLIENT_PAGE || WEBSITE_MAINTENANCE || !PAGE_EXISTS || !PAGE_PUBLISHED) {
            if (!empty(WEBSITE_KEYWORDS)) {
                return WEBSITE_KEYWORDS;
            }
        } else {
            if (!empty(PAGE_KEYWORDS)) {
                return PAGE_KEYWORDS;
            } else {
                return WEBSITE_KEYWORDS;
            }
        }
    }
}

function get_client_robots()
{
    if (WEBSITE_ROBOTS === 'noindex, nofollow' || WEBSITE_MAINTENANCE || !PAGE_EXISTS) {
        return 'noindex, nofollow';
    }

    if (!CLIENT_PAGE) {
        return 'index, follow';
    }

    if (!LOGGED_IN && !PAGE_PUBLISHED) {
        return 'noindex, nofollow';
    } else {
        return PAGE_ROBOTS;
    }
}

function get_client_icon()
{
    if (empty(WEBSITE_FAVICON)) return "#";

    return "assets/favicon/" . WEBSITE_FAVICON;
}

function get_client_header()
{
    if (!WEBSITE_HEADER || (WEBSITE_MAINTENANCE  && !LOGGED_IN)) return;

    if (PAGE_HEADER || !PAGE_EXISTS) return get_header_content();
}

function get_client_footer()
{
    if (!WEBSITE_FOOTER || (WEBSITE_MAINTENANCE  && !LOGGED_IN)) return;

    if (PAGE_FOOTER || !PAGE_EXISTS) return get_footer_content();
}
