<?php

function page_exists($URL)
{
    global $conn;

    $sql = "SELECT `id` FROM `pages` WHERE `link` = ?";

    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $URL);
    if ($stmt->execute()) {
        $stmt->bind_result($page);
        $stmt->fetch();
        $stmt->close();

        if ($page) return true;
    }
    return false;
}

function is_default_page_by_ID($page_ID)
{
    global $conn;

    $page_result = $conn->query("SELECT `link` FROM `pages` WHERE `id` = $page_ID");
    $page = $page_result->fetch_object();
    $page_result->close();

    return $page->link === WEBSITE_DEFAULT_PAGE_URL;
}

function get_not_found_content($raw = false)
{
    global $conn;

    $not_found_result = $conn->query("SELECT `content` FROM `template_pages` WHERE `name` = 'not_found'");
    $not_found = $not_found_result->fetch_object();
    $not_found_result->close();

    echo $raw ? $not_found->content : compute_short_codes($not_found->content);
}

function get_maintenance_content($raw = false)
{
    global $conn;

    $maintenance_result = $conn->query("SELECT `content` FROM `template_pages` WHERE `name` = 'maintenance'");
    $maintenance = $maintenance_result->fetch_object();
    $maintenance_result->close();

    echo $raw ? $maintenance->content : compute_short_codes($maintenance->content);
}

function get_header_content($raw = false)
{
    global $conn;

    $page_header_result = $conn->query("SELECT `content` FROM `template_components` WHERE `name` = 'header'");
    $page_header = $page_header_result->fetch_object();
    $page_header_result->close();

    echo $raw ? $page_header->content : compute_short_codes($page_header->content);
}

function get_footer_content($raw = false)
{
    global $conn;

    $page_footer_result = $conn->query("SELECT `content` FROM `template_components` WHERE `name` = 'footer'");
    $page_footer = $page_footer_result->fetch_object();
    $page_footer_result->close();

    echo $raw ? $page_footer->content : compute_short_codes($page_footer->content);
}
