<?php

$cpanel_pages = [
    'dashboard',
    'statistics',
    'pages',
    'files',
    'media-files',
    'header',
    'footer',
    'libraries',
    'shortcodes',
    'settings',
    'users',
    'profile',
    '404'
];

$login_pages = [
    'login',
    'password-recovery',
    'create-new-password'
];

if (CPANEL_ROUTE) {
    if (LOGGED_IN) {

        if (!ADMIN_PAGE) {
            redirect(('dashboard'));
        }

        if (in_array(ADMIN_PAGE, $login_pages)) {
            redirect(('dashboard'));
        }

        if (!in_array(ADMIN_PAGE, $cpanel_pages)) {
            redirect('404');
        }

        switch (ADMIN_PAGE) {
            case 'pages':
                if (ADMIN_PAGE_PARAM && ADMIN_PAGE_PARAM !== 'not-found' && ADMIN_PAGE_PARAM !== 'maintenance') {
                    if (!is_admin_page_param_valid(ADMIN_PAGE_PARAM, 'pages')) {
                        set_notification('error', 'Invalid page ID');
                        redirect('pages');
                    }
                }
                break;
            case 'files':
                if (ADMIN_PAGE_PARAM && !is_admin_page_param_valid(ADMIN_PAGE_PARAM, 'files')) {
                    set_notification('error', 'Invalid file ID');
                    redirect('files');
                }
                break;
            case 'libraries':
                if (ADMIN_PAGE_PARAM && !is_admin_page_param_valid(ADMIN_PAGE_PARAM, 'libraries')) {
                    set_notification('error', 'Invalid library ID');
                    redirect('libraries');
                }
                break;
            case 'users':
                if (!IS_ADMIN) {
                    redirect('404');
                }
                if (ADMIN_PAGE_PARAM && !is_admin_page_param_valid(ADMIN_PAGE_PARAM, 'users')) {
                    set_notification('error', 'Invalid user ID');
                    redirect('users');
                }
                break;

            default:
                break;
        }
    } else {
        if (!ADMIN_PAGE || !in_array(ADMIN_PAGE, $login_pages)) {
            redirect('login');
        }
    }
} else {
    if (CLIENT_PAGE == WEBSITE_DEFAULT_PAGE_URL) {
        redirect('', true);
    }
}
