<?php

if (!WEBSITE_MAINTENANCE || LOGGED_IN) {
    if (LOGGED_IN && PAGE_EXISTS) {
        echo compute_short_codes(PAGE_CONTENT);
    } elseif (!LOGGED_IN && PAGE_EXISTS && PAGE_PUBLISHED) {
        echo compute_short_codes(PAGE_CONTENT);
    } else {
        get_not_found_content();
    }
} else {
    get_maintenance_content();
}
