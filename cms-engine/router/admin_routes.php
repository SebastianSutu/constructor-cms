<?php

if (LOGGED_IN) {
    switch (ADMIN_PAGE) {
        case 'dashboard':
            require 'cms-engine/pages/cpanel/dashboard.php';
            break;
        case 'statistics':
            require 'cms-engine/pages/cpanel/statistics.php';
            break;
        case 'pages':
            if (ADMIN_PAGE_PARAM) {
                if (ADMIN_PAGE_PARAM === 'not-found') {
                    require 'cms-engine/pages/cpanel/edit_not_found.php';
                } elseif (ADMIN_PAGE_PARAM === 'maintenance') {
                    require 'cms-engine/pages/cpanel/edit_maintenance.php';
                } else {
                    require 'cms-engine/pages/cpanel/edit_page.php';
                }
            } else {
                require 'cms-engine/pages/cpanel/page_list.php';
            }
            break;
        case 'files':
            if (ADMIN_PAGE_PARAM) {
                require 'cms-engine/pages/cpanel/edit_file.php';
            } else {
                require 'cms-engine/pages/cpanel/file_list.php';
            }
            break;
        case 'media-files':
            require 'cms-engine/pages/cpanel/media_files.php';
            break;
        case 'header':
            require 'cms-engine/pages/cpanel/edit_header.php';
            break;
        case 'footer':
            require 'cms-engine/pages/cpanel/edit_footer.php';
            break;
        case 'libraries':
            if (ADMIN_PAGE_PARAM) {
                require 'cms-engine/pages/cpanel/edit_library.php';
            } else {
                require 'cms-engine/pages/cpanel/library_list.php';
            }
            break;
        case 'shortcodes':
            require 'cms-engine/pages/cpanel/shortcodes.php';
            break;
        case 'settings':
            require 'cms-engine/pages/cpanel/settings.php';
            break;
        case 'users':
            if (ADMIN_PAGE_PARAM) {
                require 'cms-engine/pages/cpanel/edit_user.php';
            } else {
                require 'cms-engine/pages/cpanel/users.php';
            }
            break;
        case 'profile':
            require 'cms-engine/pages/cpanel/profile.php';
            break;
        case '404':
            require 'cms-engine/pages/not_found/not_found.php';
            break;
        case 'logout':
            require 'engine/logout.php';
            break;

        default:
            break;
    }
} else {
    switch (ADMIN_PAGE) {
        case 'login':
            require 'cms-engine/pages/authentication/login.php';
            break;
        case 'password-recovery':
            require 'cms-engine/pages/authentication/password_recovery.php';
            break;
        case 'create-new-password':
            require 'cms-engine/pages/authentication/create_new_password.php';
            break;

        default:
            break;
    }
}
