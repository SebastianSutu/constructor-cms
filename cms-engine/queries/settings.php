<?php
// ============ WEBSITE ============

// website settings
if (isset($_POST['identity_submit']) && !empty($_POST)) {
    is_user_allowed();
    $title            = sanatize($_POST['site_title']);
    $description      = sanatize($_POST['site_description']);
    $keywords         = sanatize($_POST['site_keywords']);
    $author           = sanatize($_POST['site_author']);
    $robots           = isset($_POST['site_robots']) ? 'index, follow' : 'noindex, nofollow';
    $ga_code          = $_POST['ga_code'];
    $maintenance      = isset($_POST['maintenance_status']) ? 1 : 0;
    $bundle_all_files = isset($_POST['bundle_all_files']) ? 1 : 0;
    $header           = isset($_POST['header']) ? 1 : 0;
    $footer           = isset($_POST['footer']) ? 1 : 0;
    $default_page_ID  = $_POST['default_page_ID'];

    if ($bundle_all_files) {
        build_files_minified_bundle();
    } else {
        unlink('assets/css/css-bundle.minified.css');
        unlink('assets/js/js-bundle.minified.js');
    }

    $sql = "UPDATE `settings` SET `title` = ?, `description` = ?, `keywords` = ?, `author` = ?, `robots` = ?, `ga_code` = ?, `default_page_id` = ?, `maintenance` = ?, `bundle_all_files` = ?, `header` = ?, `footer` = ? WHERE `id` = 1";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('sssssssiiii', $title, $description, $keywords, $author, $robots, $ga_code, $default_page_ID, $maintenance, $bundle_all_files, $header, $footer);

    if ($stmt->execute()) {
        set_notification("success", "Website settings updated!");
    } else {
        set_notification("error", "Failed to update website settings!");
    }
    $stmt->close();
    reload();
}

// delete favicon
if (isset($_POST["delete_favicon_submit"])) {
    is_user_allowed();
    $favicon  = $_POST['favicon'];

    $sql = "UPDATE `settings` SET `favicon` = ''";
    $stmt_remove = $conn->prepare($sql);
    if ($stmt_remove->execute() && unlink('assets/favicon/' . $favicon)) {
        set_notification("success", "Icon removed!");
        $stmt_remove->close();
    } else {
        set_notification("error", "Unable to delete from database or folder!");
    }
    reload();
}

// add favicon
if (isset($_POST["add_favicon_submit"])) {
    is_user_allowed();
    // extract file information
    $imageName   = $_FILES['favicon']['name'];
    $imageSize   = $_FILES['favicon']['size'];
    $imageType   = $_FILES['favicon']['type'];
    $imageTmp    = $_FILES['favicon']['tmp_name'];
    $imageError  = $_FILES['favicon']['error'];
    $imageDest   = 'assets/favicon/' . $imageName;
    // extract file extension
    $fileExplode  = explode('.', $imageName);
    $imageExt     = strtolower(end($fileExplode));
    // allowed extensions
    $allowed  = array('ico', 'jpg', 'jpeg', 'png');
    $size     = 10 * 1024 * 1000; // 10 MB

    if ($imageError !== 0) {
        set_notification("error", "Unable to upload!");
        reload();
    }
    if (!in_array($imageExt, $allowed)) {
        set_notification("error", "Extension not allowed!");
        reload();
    }
    if ($imageSize > $size) {
        set_notification("error", "Size too big!");
        reload();
    }
    if (file_exists($imageDest)) {
        set_notification("error", 'File already exists!');
        reload();
    }

    $stmt = $conn->prepare("UPDATE `settings` SET `favicon` = ?");
    $stmt->bind_param("s", $imageName);
    if ($stmt->execute() && move_uploaded_file($imageTmp, $imageDest)) {
        set_notification("success", 'Icon uploaded!');
        $stmt->close();
    } else {
        set_notification("error", 'Unable to upload into the database or write privileges issues!');
    }
    reload();
}
