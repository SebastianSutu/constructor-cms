<?php
// ============ PAGES ADD / UPDATE / REMOVE ============

// add page
if (isset($_POST['add_page']) && !empty($_POST)) {
    $page_name    = filter_page_title(sanatize($_POST['page_name']));
    $page_url     = filter_page_url(sanatize($_POST['page_URL']));
    $page_status  = isset($_POST['page_status']) ? 1 : 0;
    $page_author  = USER_USERNAME;

    $banned_page_urls = ['assets', 'cms-admin', 'cms-engine'];

    if (in_array($page_url, $banned_page_urls)) {
        set_notification("error", "Reserved URL!");
        reload();
    }

    if (empty($page_name) || empty($page_url)) {
        set_notification("error", "Empty page title or URL!");
        reload();
    }

    if (page_exists($page_url)) {
        set_notification("error", "Page already exists!");
        reload();
    }

    $sql = "INSERT INTO `pages` (`name`, `link`, `created`, `author`, `status`) VALUES (?,?,?,?,?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ssssi', $page_name, $page_url, $current_date_time, $page_author, $page_status);

    if ($stmt->execute()) {
        set_notification('success', "Added page: <b>$page_name</b>");
        $last_inserted_id = $conn->insert_id;
        redirect("pages/$last_inserted_id");
    } else {
        set_notification("error", "There was an erorr adding the page: <b>$page_name</b>!");
        reload();
    }
    $stmt->close();
}

// edit page information
if (isset($_POST['edit_page_submit']) && !empty($_POST)) {
    $page_ID          = $_POST['page_ID'];
    $old_page_title   = $_POST['old_page_title'];
    $old_page_url     = $_POST['old_page_url'];
    $page_title       = filter_page_title(sanatize($_POST['page_title']));
    $page_url         = filter_page_url(sanatize($_POST['page_url']));
    $page_description = trim(preg_replace('/\s+/', ' ', $_POST['page_description']));
    $page_keywords    = sanatize($_POST['page_keywords']);
    $page_robots      = isset($_POST['page_robots']) ? 'index, follow' : 'noindex, nofollow';
    $old_page_header  = isset($_POST['old_header']) ? 1 : 0;
    $old_page_footer  = isset($_POST['old_footer']) ? 1 : 0;
    $page_header      = isset($_POST['header']) ? 1 : 0;
    $page_footer      = isset($_POST['footer']) ? 1 : 0;
    $page_author      = $_POST['page_author'];

    if ((!WEBSITE_HEADER && $old_page_header != $page_header) || (!WEBSITE_FOOTER && $old_page_footer != $page_footer)) {
        set_notification("error", "You're not allowed to do this!");
        reload();
    }

    if (!IS_ADMIN) {
        if (USER_USERNAME !== $page_author) {
            set_notification("warning", "You're not allowed to do this!");
            reload();
        }
    }

    if ($old_page_url !== $page_url) {
        if (page_exists($page_url)) {
            set_notification("error", "That URL is already taken!");
            reload();
        }
    }

    if (empty($page_title) || empty($page_url)) {
        set_notification("error", "Empty page title or URL!");
        reload();
    }

    $sql = "UPDATE `pages` SET `name` = ?, `link` = ?, `description` = ?, `keywords` = ?, `robots` = ?, `header` = ?, `footer` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssiii", $page_title, $page_url, $page_description, $page_keywords, $page_robots, $page_header, $page_footer, $page_ID);
    if ($stmt->execute()) {
        set_notification("success", "Page information updated!");
    } else {
        set_notification("error", "There was an error!");
    }
    reload();
}


// toggle page status from page list
if (isset($_POST['update_page_status']) && !empty($_POST)) {
    $page_ID = $_POST['page_ID'];
    $page_name = $_POST['page_name'];
    $page_status = isset($_POST['page_status']) ? 1 : 0;

    if (is_default_page_by_ID($page_ID)) {
        set_notification("error", "Homepage status can't be disabled");
        reload();
    }

    $session_type = $page_status ? 'success' : 'warning';
    $session_message = $page_status ? "Enabled page: <b>$page_name</b>" : "Disabled page: <b>$page_name</b>";

    $sql  = "UPDATE `pages` SET `status` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ii', $page_status, $page_ID);

    if ($stmt->execute()) {
        set_notification($session_type, $session_message);
    } else {
        set_notification('error', "Failed to update page status of: <b>$page_name</b>!");
    }
    $stmt->close();
    reload();
}

// remove page
if (isset($_POST['remove_page']) && !empty($_POST)) {
    $page_ID     = $_POST['page_ID'];
    $page_name   = $_POST['page_name'];
    $page_author = $_POST['page_author'];
    $redirect    = isset($_POST['redirect']) ? true : false;

    if (!IS_ADMIN) {
        if (USER_USERNAME !== $page_author) {
            set_notification("warning", "You're not allowed to do this!");
            reload();
        }
    }

    if (is_default_page_by_ID($page_ID)) {
        set_notification("error", "Homepage can't be removed!");
        reload();
    }

    $sql = "DELETE FROM `pages` WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $page_ID);

    if ($stmt->execute()) {
        set_notification("success", "Removed page: <b>$page_name</b>");
    } else {
        set_notification("error", "There was an erorr removing the page!");
    }
    $stmt->close();
    $redirect ? redirect('pages') : reload();
}

// monaco editor page update
if (isset($_POST['editor_update_submit']) && !empty($_POST)) {
    $editor_value = $_POST['editor_value'];
    $page_link    = $_POST['page_link'];

    $sql  = "UPDATE `pages` SET `content` = ?, `modified` = ? WHERE `link` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('sss', $editor_value, $current_date_time, $page_link);
    if ($stmt->execute()) {
        set_notification("success", "Code updated!");
    } else {
        set_notification("error", "Failed to update!");
    }
    $stmt->close();
}

// edit header content
if (isset($_POST['template_update_header_submit']) && !empty($_POST)) {
    $header_content = $_POST['editor_value'];

    $sql  = "UPDATE `template_components` SET `content` = ? WHERE `name` = 'header'";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $header_content);
    if ($stmt->execute()) {
        set_notification("success", "Code updated!");
    } else {
        set_notification("error", "Failed to update!");
    }
    $stmt->close();
}

// edit footer content
if (isset($_POST['template_update_footer_submit']) && !empty($_POST)) {
    $footer_content = $_POST['editor_value'];

    $sql  = "UPDATE `template_components` SET `content` = ? WHERE `name` = 'footer'";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $footer_content);
    if ($stmt->execute()) {
        set_notification("success", "Code updated!");
    } else {
        set_notification("error", "Failed to update!");
    }
    $stmt->close();
}

// edit maintenance page content
if (isset($_POST['template_update_maintenance_submit']) && !empty($_POST)) {
    $maintenance_content = $_POST['editor_value'];

    $sql  = "UPDATE `template_pages` SET `content` = ? WHERE `name` = 'maintenance'";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $maintenance_content);
    if ($stmt->execute()) {
        set_notification("success", "Code updated!");
    } else {
        set_notification("error", "Failed to update!");
    }
    $stmt->close();
}

// edit not found page content
if (isset($_POST['template_update_not_found_submit']) && !empty($_POST)) {
    $not_found_content = $_POST['editor_value'];

    $sql  = "UPDATE `template_pages` SET `content` = ? WHERE `name` = 'not_found'";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $not_found_content);
    if ($stmt->execute()) {
        set_notification("success", "Code updated!");
    } else {
        set_notification("error", "Failed to update!");
    }
    $stmt->close();
}
