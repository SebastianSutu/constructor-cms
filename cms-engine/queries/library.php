<?php

// order libraries
if (isset($_POST['order_libraries']) && !empty($_POST['order'])) {
    $order = $_POST['order'];
    $order = explode('&', $order);

    foreach ($order as $position => $item_id) {
        $item_id = trim($item_id, 'item[]=');
        $item_id = (int) $item_id;
        $conn->query("UPDATE `libraries` SET `position` = $position WHERE `id` = $item_id");
    }
    die;
}

// add library
if (isset($_POST['add_library']) && !empty($_POST)) {
    $library_name = sanatize($_POST['library_name']);
    $library_URL = $_POST['library_URL'];
    $library_crossorigin = $_POST['library_crossorigin'];
    $library_integrity = $_POST['library_integrity'];
    $library_type = $_POST['library_type'];
    $library_status = isset($_POST['library_status']) ? 1 : 0;

    $position_query = $conn->query("SELECT MAX(`position`) AS 'position' FROM `libraries` WHERE `type` = '$library_type'");
    $position_result = $position_query->fetch_object();
    $position_result->position;
    $position = $position_result->position + 1;

    $sql = "INSERT INTO `libraries` (`name`, `url`, `crossorigin`, `integrity`, `type`, `status`, `position`) VALUES (?,?,?,?,?,?,?) ";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('sssssii', $library_name, $library_URL, $library_crossorigin, $library_integrity, $library_type, $library_status, $position);

    if (empty($library_name) || empty($library_URL)) {
        set_notification('error', 'Empty library name or URL!');
        reload();
    }

    if ($stmt->execute()) {
        set_notification('success', "Added Library: <b>$library_name</b>");
    } else {
        set_notification('error', "Failed to add library: <b>$library_name</b>!");
    }
    $stmt->close();
    reload();
}

// toggle library status from library list
if (isset($_POST['update_library_status']) && !empty($_POST)) {
    $library_ID = $_POST['library_ID'];
    $library_name = $_POST['library_name'];
    $library_status = isset($_POST['library_status']) ? 1 : 0;

    $session_type = $library_status ? 'success' : 'warning';
    $session_message = $library_status ? "Enabled library: <b>$library_name</b>" : "Disabled library: <b>$library_name</b>";

    $sql  = "UPDATE `libraries` SET `status` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ii', $library_status, $library_ID);

    if ($stmt->execute()) {
        set_notification($session_type, $session_message);
    } else {
        set_notification('error', "Failed to update file status of: <b>$file_name.$file_type</b>!");
    }
    $stmt->close();
    reload();
}

// remove library
if (isset($_POST['remove_library']) && !empty($_POST)) {
    $library_ID = $_POST['library_ID'];
    $library_name = $_POST['library_name'];

    $sql  = "DELETE FROM `libraries` WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $library_ID);

    if ($stmt->execute()) {
        set_notification('success', "Removed library: <b>$library_name</b>");
    } else {
        set_notification('error', "Failed to remove library: <b>$library_name</b>!");
    }
    $stmt->close();
    reload();
}

// edit library
if (isset($_POST['library_edit_submit']) && !empty($_POST)) {
    $library_ID = $_POST['library_ID'];
    $library_name = sanatize($_POST['library_name']);
    $library_URL = $_POST['library_URL'];
    $library_crossorigin = $_POST['library_crossorigin'];
    $library_integrity = $_POST['library_integrity'];
    $library_status = isset($_POST['library_status']) ? 1 : 0;

    if (empty($library_name) || empty($library_URL)) {
        set_notification('error', 'Empty library name or URL!');
        reload();
    }

    $sql = "UPDATE `libraries` SET `name` = ?, `url` = ?, `crossorigin` = ?, `integrity` = ?, `status` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ssssii', $library_name, $library_URL, $library_crossorigin, $library_integrity, $library_status, $library_ID);

    if ($stmt->execute()) {
        set_notification('success', "Updated library: <b>$library_name</b>");
    } else {
        set_notification('error', "Failed to update library: <b>$library_name</b>!");
    }
    $stmt->close();
    reload();
}
