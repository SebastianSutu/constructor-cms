<?php
// ============ ADD USER / LOGIN / PASSWORD CHANGE & RECOVERY ============

// add user
if (isset($_POST["add_user_submit"])) {
    $fullname        = sanatize($_POST['new_user_fullname']);
    $username        = sanatize($_POST['new_user_username']);
    $email           = sanatize_email($_POST['new_user_email']);
    $password        = sanatize($_POST['new_user_password']);
    $password_repeat = sanatize($_POST['new_user_password_repeat']);
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);
    $account_type    = isset($_POST['new_user_type']) ? 2 : 1;
    $last_activity   = null;

    if (empty($fullname) || empty($username) || empty($password) || empty($password_repeat)) {
        set_notification('error', 'Empty fields!');
        reload();
    }
    if ($password !== $password_repeat) {
        set_notification('error', "Passwords don't match!");
        reload();
    }

    // check if username is taken
    $stmt = $conn->prepare("SELECT `username` FROM `users` WHERE `username` = ?");
    $stmt->bind_param("s", $username);

    if ($stmt->execute()) {
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            set_notification('error', "Username is already taken!");
            reload();
        }
        $stmt->close();
    }


    // check if email is taken
    if ($email) {
        $stmt = $conn->prepare("SELECT `email` FROM `users` WHERE `email` = ?");
        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $stmt->store_result();
            if ($stmt->num_rows > 0) {
                set_notification('error', "Email is already taken!");
                reload();
            }
            $stmt->close();
        }
    }

    // insert into db the new user
    $stmt = $conn->prepare("INSERT INTO `users` (`fullname`, `username`, `email`, `password`, `type`, `last_activity`) VALUES(?,?,?,?,?,?)");
    $stmt->bind_param("sssssi", $fullname, $username, $email, $hashed_password, $account_type, $last_activity);
    if ($stmt->execute()) {
        set_notification('success', 'User created successfully!');
    }
    $stmt->close();
    reload();
}

// remove user
if (isset($_POST["remove_user_submit"])) {
    is_user_allowed();

    $user_ID = $_POST['user_ID'];

    if (USER_ID == $user_ID) {
        set_notification('error', "You cannot delete yourself!");
        reload();
    }

    $stmt = $conn->prepare("DELETE FROM `users` WHERE `id` = ?");
    $stmt->bind_param("i", $user_ID);
    if ($stmt->execute()) {
        set_notification('success', "User removed!");
    } else {
        set_notification('error', "There was an error removing the user!");
    }
    $stmt->close();
    reload();
}


// admin panel user edit
if (isset($_POST["admin_panel_user_edit"]) && !empty($_POST)) {
    $user_ID           = $_POST['user_ID'];
    $user_type         = $_POST['user_type'];
    $user_fullname     = sanatize($_POST['user_fullname']);
    $user_email        = sanatize_email($_POST['user_email']);

    if (strlen($user_fullname) < 5) {
        set_notification('warning', "Username too short!");
        reload();
    }

    // check if email is taken
    $stmt = $conn->prepare("SELECT `email` FROM `users` WHERE `email` = '$user_email' AND `id` != $user_ID");
    if ($stmt->execute()) {
        $stmt->bind_result($is_email);
        $stmt->fetch();
        $stmt->close();

        if (!empty($is_email)) {
            set_notification('error', "Email is already taken!");
            reload();
        }
    }

    $stmt = $conn->prepare("UPDATE `users` SET `fullname` = ?, `email` = ?, `type` = ? WHERE `id` = $user_ID");
    $stmt->bind_param("ssi", $user_fullname, $user_email, $user_type);
    if ($stmt->execute()) {
        set_notification('success', "User info updated!");
    } else {
        set_notification('error', "There was an error!");
    }
    $stmt->close();
    reload();
}


// edit user profile
if (isset($_POST["edit_user_info_submit"]) && !empty($_POST)) {
    $post_fullname     = sanatize($_POST['user_fullname']);
    $post_email        = sanatize_email($_POST['user_email']);

    if (strlen($post_fullname) < 5) {
        set_notification('warning', "Username too short!");
        reload();
    }

    $email_result = $conn->query("SELECT `email` FROM `users` WHERE `id` = " . USER_ID);
    $user_email = $email_result->fetch_object();

    if ($user_email->email !== $post_email) {
        // check if email is taken
        $stmt = $conn->prepare("SELECT `email` FROM `users` WHERE `email` = ?");
        $stmt->bind_param("s", $post_email);

        if ($stmt->execute()) {
            $stmt->bind_result($is_email);
            $stmt->fetch();
            $stmt->close();

            if (!empty($is_email)) {
                set_notification('error', "Email is already taken!");
                reload();
            }
        }
    }

    $stmt = $conn->prepare("UPDATE `users` SET  `fullname` = ?, `email` = ? WHERE `id` = " . USER_ID);
    $stmt->bind_param("ss", $post_fullname, $post_email);
    if ($stmt->execute()) {
        set_notification('success', "User information updated!");
    } else {
        set_notification('error', "There was an error!");
    }
    $stmt->close();
    reload();
}
