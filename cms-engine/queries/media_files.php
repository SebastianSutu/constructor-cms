<?php
// ============ MEDIA FILES ============

// upload media files
if (isset($_POST['media_file_upload_submit']) && !empty($_POST)) {
    $files = array_filter($_FILES['media_file']['name']);
    $total = count($files);

    for ($i = 0; $i < $total; $i++) {
        $media_file_tmp = $_FILES['media_file']['tmp_name'][$i];
        $media_file_path = "assets/media/" . $_FILES['media_file']['name'][$i];

        if (file_exists($media_file_path)) {
            set_notification('error', 'File already exists');
            reload();
        }

        if (!empty($media_file_tmp)) {
            if (move_uploaded_file($media_file_tmp, $media_file_path)) {
                set_notification('success', 'Files uploaded successfully');
                reload();
            }
        }
    }
    reload();
}

// remove media files
if (isset($_POST['media_file_delete_submit']) && !empty($_POST)) {
    is_user_allowed();
    $media_file_name = $_POST['media_file_name'];
    $media_file_path = "assets/media/$media_file_name";

    if (unlink($media_file_path)) {
        set_notification('success', 'File deleted successfully');
    } else {
        set_notification('error', 'There was an error deleting the file');
    }
    reload();
}
