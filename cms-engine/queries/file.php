<?php
// ============ FILES ============

// order files
if (isset($_POST['order_files']) && !empty($_POST['order'])) {
    $order = $_POST['order'];
    $order = explode('&', $order);

    foreach ($order as $position => $item_id) {
        $item_id = trim($item_id, 'item[]=');
        $item_id = (int) $item_id;
        $conn->query("UPDATE `files` SET `position` = $position WHERE `id` = $item_id");
    }
    die;
}

// add file CSS / JS
if (isset($_POST['add_file_submit']) && !empty($_POST)) {
    $file_name    = filter_file_name(sanatize($_POST['file_name']));
    $file_type    = $_POST['file_type'];
    $file_author  = USER_USERNAME;
    $file_status  = isset($_POST['file_status']) ? 1 : 0;
    $file_path    = "assets/$file_type/$file_name.$file_type";
    $file_path_minified = "assets/$file_type/$file_name.minified.$file_type";

    if (empty($file_name)) {
        set_notification('error', "Empty file name!");
        reload();
    }
    if (file_exists($file_path)) {
        set_notification('error', "File: <b>$file_name.$file_type</b> already exists!");
        reload();
    }

    $banned_file_names = ['css-bundle.minified', 'js-bundle.minified'];
    if (in_array($file_name, $banned_file_names)) {
        set_notification('error', "Reserved file name!");
        reload();
    }

    $position_query = $conn->query("SELECT MAX(`position`) AS 'position' FROM `files` WHERE `type` = '$file_type'");
    $position_result = $position_query->fetch_object();
    $position_result->position;
    $position = $position_result->position + 1;

    $sql = "INSERT INTO `files` (`name`, `type`, `created`, `author`, `status`, `position`) VALUES (?,?,?,?,?,?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ssssii', $file_name, $file_type, $current_date_time, $file_author, $file_status, $position);

    if ($stmt->execute() && fopen($file_path, 'w') && fopen($file_path_minified, 'w')) {
        set_notification("success", "Added file: <b>$file_name.$file_type</b>");
        $last_inserted_id = $conn->insert_id;
    } else {
        set_notification("error", "Failed to create file: <b>$file_name.$file_type</b>");
    }
    $stmt->close();
    reload();
}

// edit file
if (isset($_POST['edit_file_submit']) && !empty($_POST)) {
    $file_ID         = $_POST['file_ID'];
    $file_type       = $_POST['file_type'];
    $file_author     = $_POST['file_author'];
    $old_file_name   = $_POST['old_file_name'];
    $file_name       = filter_file_name(sanatize($_POST['file_name']));
    $old_minify_status = $_POST['old_minify'];
    $minify_status   = isset($_POST['minify']) ? 1 : 0;

    $file_path       = "assets/$file_type/$old_file_name.$file_type";
    $new_file_path   = "assets/$file_type/$file_name.$file_type";
    $file_path_minified       = "assets/$file_type/$old_file_name.minified.$file_type";
    $new_file_path_minified   = "assets/$file_type/$file_name.minified.$file_type";

    if (!IS_ADMIN) {
        if (USER_USERNAME !== $file_author) {
            set_notification('warning', "You're not allowed to do this!");
            reload();
        }
    }

    if (WEBSITE_BUNDLE_ALL_FILES && $old_minify_status != $minify_status) {
        set_notification('error', "Unable to toggle file minification when 'Bundle all CSS and JS files' is enabled in Settings");
        reload();
    }

    if (empty($file_name)) {
        set_notification('error', "Empty file name!");
        reload();
    }

    if ($old_file_name !== $file_name) {
        if (file_exists($new_file_path)) {
            set_notification('error', "File name: <b>$file_name.$file_type</b> already exists!");
            reload();
        }
    }

    $sql = "UPDATE `files` SET `name` = ?, `minify` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sii", $file_name, $minify_status, $file_ID);

    if ($stmt->execute() && rename($file_path, $new_file_path) && rename($file_path_minified, $new_file_path_minified)) {
        set_notification("success", "File information updated!");
    } else {
        set_notification("error", "There was an error!");
    }
    reload();
}

// toggle file status from file list
if (isset($_POST['update_file_status']) && !empty($_POST)) {
    $file_ID = $_POST['file_ID'];
    $file_name = $_POST['file_name'];
    $file_type = $_POST['file_type'];
    $file_status = isset($_POST['file_status']) ? 1 : 0;

    $session_type = $file_status ? 'success' : 'warning';
    $session_message = $file_status ? "Enabled file: <b>$file_name.$file_type</b>" : "Disabled file: <b>$file_name.$file_type</b>";

    $sql  = "UPDATE `files` SET `status` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ii', $file_status, $file_ID);

    if ($stmt->execute()) {
        set_notification($session_type, $session_message);
    } else {
        set_notification('error', "Failed to update file status of: <b>$file_name.$file_type</b>!");
    }
    $stmt->close();

    build_files_minified_bundle();

    reload();
}

// toggle file minification
if (isset($_POST['update_file_minify_status']) && !empty($_POST)) {
    if (WEBSITE_BUNDLE_ALL_FILES) {
        set_notification('error', "Unable to toggle file minification when 'Bundle all CSS and JS files' is enabled in Settings");
        reload();
    }

    $file_ID = $_POST['file_ID'];
    $file_name = $_POST['file_name'];
    $file_type = $_POST['file_type'];
    $minify = isset($_POST['minify']) ? 1 : 0;

    $session_type = $minify ? 'success' : 'warning';
    $session_message = $minify ? "Enabled minification for file: <b>$file_name.$file_type</b>" : "Disabled minification for file: <b>$file_name.$file_type</b>";

    $sql  = "UPDATE `files` SET `minify` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ii', $minify, $file_ID);

    if ($stmt->execute()) {
        set_notification($session_type, $session_message);
    } else {
        set_notification('error', "Failed to update file minify status of: <b>$file_name.$file_type</b>!");
    }
    $stmt->close();

    reload();
}

// remove file CSS / JS
if (isset($_POST['remove_file']) && !empty($_POST)) {
    $file_ID     = $_POST['file_ID'];
    $file_name   = $_POST['file_name'];
    $file_type   = $_POST['file_type'];
    $file_author = $_POST['file_author'];
    $file_path   = "assets/$file_type/$file_name.$file_type";
    $file_path_minified = "assets/$file_type/$file_name.minified.$file_type";
    $redirect    = isset($_POST['redirect']) ? true : false;

    if (!IS_ADMIN) {
        if (USER_USERNAME !== $file_author) {
            set_notification('warning', "You're not allowed to do this!");
            reload();
        }
    }

    if (!file_exists($file_path)) {
        set_notification('error', "File doesn't exist anymore!");
        reload();
    }

    $sql = "DELETE FROM `files` WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $file_ID);

    if ($stmt->execute() && unlink($file_path) && unlink($file_path_minified)) {
        set_notification('success', "Removed file: <b>$file_name.$file_type</b>");
        $stmt->close();
    } else {
        set_notification('error', "There was an erorr removing the file: <b>$file_name.$file_type</b>!");
    }

    build_files_minified_bundle();

    $redirect ? redirect('files') : reload();
}

// monaco editor file update
if (isset($_POST['file_update_submit']) && !empty($_POST)) {
    $file_ID      = $_POST['file_ID'];
    $file_name    = $_POST['file_name'];
    $file_type    = $_POST['file_type'];
    $editor_value = $_POST['editor_value'];
    $file_path    = "assets/$file_type/$file_name.$file_type";
    $file_path_minified = "assets/$file_type/$file_name.minified.$file_type";

    $sql  = "UPDATE `files` SET `modified` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('si', $current_date_time, $file_ID);
    if ($stmt->execute()) {
        set_notification('success', 'File content updated');
    } else {
        set_notification('error', 'Failed to update file content');
    }
    $stmt->close();

    $minified_value = $file_type == 'css' ? minify_CSS($editor_value) : minify_JS($editor_value);

    if (file_put_contents($file_path, $editor_value) && file_put_contents($file_path_minified, $minified_value)) {
        build_files_minified_bundle();

        return "Code updated";
    } else {
        return "error";
    }
}
