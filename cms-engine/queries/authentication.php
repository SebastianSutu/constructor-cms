<?php
// ============ PASSWORD RESET / RECOVERY ============

// password reset
if (isset($_POST['password_reset_submit']) && !empty($_POST)) {
    $user_selector        = $_POST['user_selector'];
    $user_validator       = $_POST['user_validator'];
    $user_password        = $_POST['user_password'];
    $user_password_repeat = $_POST['user_password_repeat'];
    $hashed_user_password = password_hash($user_password, PASSWORD_DEFAULT);

    $current_date = date('U');
    $token_binary = hex2bin($user_validator);

    if ($user_password !== $user_password_repeat) {
        $_SESSION['error'] = 'Passwords dont match!';
        return;
    }

    $sql  = "SELECT `email`, `token` FROM `password_reset` WHERE `selector` = ? AND `expires` >= ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('ss', $user_selector, $current_date);
    $stmt->execute();
    $stmt->bind_result($email, $token);
    $stmt->fetch();
    $stmt->close();

    if (empty($email)) {
        $_SESSION['error'] = 'Your session has expired!';
        return;
    }
    if (!password_verify($token_binary, $token)) {
        $_SESSION['error'] = 'There was an error resetting your password';
        return;
    }

    $sql  = "SELECT `id` FROM `users` WHERE `email` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $stmt->bind_result(USER_ID);
    $stmt->fetch();
    $stmt->close();

    if (empty(USER_ID)) {
        $_SESSION['error'] = 'No account to reset the password to!';
        return;
    }

    $sql  = "UPDATE `users` SET `password` = ? WHERE `id` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('si', $hashed_user_password, USER_ID);
    $stmt->execute();
    $stmt->close();

    $sql  = "DELETE FROM `password_reset` WHERE `email` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $stmt->close();

    $_SESSION['success'] = 'Successfully reset the password';
    redirect('login');
}

// password reset request
if (isset($_POST['password_reset_request']) && !empty($_POST)) {
    $selector     = bin2hex(random_bytes(8));
    $token        = random_bytes(32);
    $hashedToken  = password_hash($token, PASSWORD_DEFAULT);
    $expires      = date("U") + 1800;
    $user_email   = sanatize_email($_POST['user_email']);

    $sql  = "SELECT `email` FROM `users` WHERE `email` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('s', $user_email);
    $stmt->execute();
    $stmt->bind_result($email);
    $stmt->fetch();
    $stmt->close();

    if (empty($email)) {
        $_SESSION['error'] = 'There is no account with this email!';
        return;
    }

    if (isset($_SERVER['HTTPS'])) {
        $url = "https://" . $_SERVER['HTTP_HOST'] . "/cms-admin/create-new-password?selector=$selector&validator=" . bin2hex($token);
    } else {
        $url = "http://" . $_SERVER['HTTP_HOST'] . "/cms-admin/create-new-password?selector=$selector&validator=" . bin2hex($token);
    }

    $sql = "DELETE FROM `password_reset` WHERE `email` = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $user_email);
    $stmt->execute();

    $sql = "INSERT INTO `password_reset` (`email`, `selector`, `token`, `expires`) VALUES (?,?,?,?)";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssss", $user_email, $selector, $hashedToken, $expires);
    $stmt->execute();

    $to = $user_email;
    $subject = "Password Recovery";

    $message  = "<p>We received a request to reset the password for the account $user_email.</p><br>";
    $message .= "<p>Click the link below to reset your password:</p>";
    $message .= "<p><a href='$url'>Reset Password</a></p>";
    $message .= "<br><br>";
    $message .= "<p>If this wasn't you, please ignore this message!</p>";

    $headers = "From: " . WEBSITE_TITLE . " <office@2fish.ro>\r\n";
    $headers .= "Reply-To: <office@2fish.ro>\r\n";
    $headers .= "Content-type: text/html\r\n";

    mail($to, $subject, $message, $headers);

    $_SESSION['success'] = "Success! Please check your email to reset your password";
    redirect('login');
}

// ============ PASSWORD CHANGE ============

// change password
if (isset($_POST["change_password_submit"]) && !empty($_POST)) {
    $old_password        = sanatize($_POST['old_password']);
    $new_password        = sanatize($_POST['new_password']);
    $new_password_repeat = sanatize($_POST['new_password_repeat']);

    $stmt = $conn->prepare("SELECT `password` FROM `users` WHERE `id` = " . USER_ID);
    $stmt->execute();
    $stmt->bind_result($current_password);
    $stmt->fetch();
    $stmt->close();

    if (empty($old_password && $new_password && $new_password_repeat)) {
        $_SESSION["error"] = 'Empty fields!';
    }
    if (!password_verify($old_password, $current_password)) {
        $_SESSION["error"] = 'Incorrect old password!';
    }
    if ($new_password !== $new_password_repeat) {
        $_SESSION["error"] = 'Passwords dont match!';
    }
    if (!empty($_SESSION['error'])) {
        return;
    }

    $new_password_hash = password_hash($new_password, PASSWORD_DEFAULT);
    $password_change = $conn->prepare("UPDATE `users` SET `password` = ? WHERE `id` = " . USER_ID);
    $password_change->bind_param("s", $new_password_hash);
    if ($password_change->execute()) {
        $_SESSION["success"] = 'Password changed successfully!';
        $password_change->close();
    }
    reload();
}

// ============ LOGIN / LOGOUT ============

// login
if (isset($_POST["login"])) {
    $post_username = sanatize($_POST['username']);
    $post_password = sanatize($_POST['password']);

    $stmt = $conn->prepare("SELECT `id`, `username`, `fullname`, `email`, `password` FROM `users` WHERE `username` = ? OR `email` = ?");
    $stmt->bind_param("ss", $post_username, $post_username);

    if ($stmt->execute()) {
        $stmt->bind_result($id, $username, $fullname, $email, $password);
        $stmt->fetch();
        $stmt->close();

        if (filter_var($post_username, FILTER_VALIDATE_EMAIL)) {
            if ($post_username !== $email) {
                $_SESSION["warning"] = 'Wrong email!';
                return;
            }
        } else {
            if ($post_username !== $username) {
                $_SESSION["warning"] = 'Wrong username!';
                return;
            }
        }

        if (!password_verify($post_password, $password)) {
            $_SESSION["warning"] = 'Wrong password!';
            return;
        }

        $_SESSION['id'] = $id;
        $_SESSION["success"] = "Welcome back, <b>$fullname</b> !";
        $last_activity = $conn->query("UPDATE `users` SET `last_activity` = '$current_date_time' WHERE `username` = '$post_username'");
        redirect('dashboard');
    } else {
        $_SESSION["error"] = 'There was an error!';
    }
}

// logout
if (isset($_POST['logout'])) {
    logout();
}
