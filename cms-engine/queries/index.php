<?php

require "authentication.php";

if (LOGGED_IN) {
    require "file.php";
    require "library.php";
    require "media_files.php";
    require "page.php";
    require "user.php";
    require "settings.php";
}
