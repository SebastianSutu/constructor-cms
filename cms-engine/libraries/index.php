<?php

require_once 'minifier/minify/src/Minify.php';
require_once 'minifier/minify/src/CSS.php';
require_once 'minifier/minify/src/JS.php';
require_once 'minifier/minify/src/Exception.php';
require_once 'minifier/minify/src/Exceptions/BasicException.php';
require_once 'minifier/minify/src/Exceptions/FileImportException.php';
require_once 'minifier/minify/src/Exceptions/IOException.php';
require_once 'minifier/path-converter/src/ConverterInterface.php';
require_once 'minifier/path-converter/src/Converter.php';
