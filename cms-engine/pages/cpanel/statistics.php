<?php

$sql = "SELECT 
    (SELECT COUNT(*) FROM `pages`) AS 'number_of_pages', 
    (SELECT COUNT(*) FROM `files`) AS 'number_of_files', 
    (SELECT COUNT(*) FROM `users`) AS 'number_of_users'
";

$count_results = $conn->query($sql);
$counts = $count_results->fetch_object();
$count_results->close();

$media_files = new FilesystemIterator('assets/media/', FilesystemIterator::SKIP_DOTS);
$media_files_count = iterator_count($media_files) - 1;

?>

<h5 class="mb-4 text-secondary">
    Statistics
</h5>

<div class="row mb-3">
    <div class="col-xl-4 col-lg-6 py-2">
        <div class="card">
            <div class="card-body">
                <i class="fas fa fa-copy fa-4x"></i>
                <h6 class="text-muted">Total created pages</h6>
                <h1 class="display-5"><?= $counts->number_of_pages ?></h1>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-6 py-2">
        <div class="card">
            <div class="card-body">
                <i class="fas fa-file-code fa-4x"></i>
                <h6 class="text-muted">Total created CSS & JS files</h6>
                <h1 class="display-5"><?= $counts->number_of_files ?></h1>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-6 py-2">
        <div class="card">
            <div class="card-body">
                <i class="fas fa-users fa-4x"></i>
                <h6 class="text-muted">Users in the platform</h6>
                <h1 class="display-5"><?= $counts->number_of_users ?></h1>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-6 py-2">
        <div class="card">
            <div class="card-body">
                <i class="fas fa-paperclip fa-4x"></i>
                <h6 class="text-muted">Media files</h6>
                <h1 class="display-5"><?= $media_files_count ?></h1>
            </div>
        </div>
    </div>
</div>