<h5 class="mb-4 text-secondary">
    Shortcodes
</h5>

<table class="table">
    <tr>
        <th>
            <code>
                {{WEBSITE_TITLE}}
            </code>
        </th>
        <td><?= WEBSITE_TITLE ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_DESCRIPTION}}
            </code>
        </th>
        <td><?= WEBSITE_DESCRIPTION ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_KEYWORDS}}
            </code>
        </th>
        <td><?= WEBSITE_KEYWORDS ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_AUTHOR}}
            </code>
        </th>
        <td><?= WEBSITE_AUTHOR ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_ROBOTS}}
            </code>
        </th>
        <td><?= WEBSITE_ROBOTS ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_FAVICON}}
            </code>
        </th>
        <td><?= WEBSITE_FAVICON ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_MAINTENANCE}}
            </code>
        </th>
        <td><?= WEBSITE_MAINTENANCE ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_DEFAULT_PAGE_URL}}
            </code>
        </th>
        <td><?= WEBSITE_DEFAULT_PAGE_URL ?></td>
    </tr>
    <tr>
        <th>
            <code>
                {{WEBSITE_GA_CODE}}
            </code>
        </th>
        <td><?= WEBSITE_GA_CODE ?></td>
    </tr>
</table>