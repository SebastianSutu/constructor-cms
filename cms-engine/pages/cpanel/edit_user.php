<?php
$user_ID = ADMIN_PAGE_PARAM;

$user_result = $conn->query("SELECT * FROM users WHERE id = $user_ID");
$user = $user_result->fetch_object();

?>

<h5 class="mb-4 text-secondary">
    <a class="text-dark" href="users">User Settings</a> / <?= $user->fullname ?> (<?= $user->username ?>)
</h5>

<div class="card">
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-group">
                <label>Full Name</label>
                <input class="form-control" name="user_fullname" value="<?= $user->fullname ?>">
            </div>
            <div class="form-group">
                <label>Username</label>
                <input class="form-control" name="user_username" value="<?= $user->username ?>" readonly>
            </div>
            <div class="form-group">
                <label>E-Mail</label>
                <input type="email" class="form-control" name="user_email" value="<?= $user->email ?>">
            </div>
            <div class="form-group">
                <label>Account Type</label>
                <select class="form-control" name="user_type">
                    <option value="1" <?= $user->type == 1 ? 'selected'  : null ?>>user</option>
                    <option value="2" <?= $user->type == 2 ? 'selected'  : null ?>>admin</option>
                </select>
            </div>
            <input type="hidden" name="user_ID" value="<?= $user_ID ?>">
            <button type="submit" class="btn btn-dark btn-sm" name="admin_panel_user_edit">Submit</button>
        </form>
    </div>
</div>