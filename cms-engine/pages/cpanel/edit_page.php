<?php
$page_ID = ADMIN_PAGE_PARAM;
$page_result = $conn->query("SELECT * FROM `pages` WHERE `id` = $page_ID");
$page = $page_result->fetch_object();

require 'cms-engine/template/components/modals/edit_page.php';
?>

<div id="is_edit_page_enabled"></div>

<div class="d-flex justify-content-between">
    <h5 class="mb-4 text-secondary">
        <a class="text-dark" href="pages">Pages</a> / <?= $page->name ?>
    </h5>
</div>

<div class="d-flex flex-column">

    <div id="editorSuccess"></div>

    <div class="page-actions d-flex justify-content-between">
        <div class="btn-group">
            <a href="<?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? '../' : '../' . $page->link ?>" target="_blank" class="btn btn-dark"><i class="fas fa-external-link-alt"></i> View</a>
        </div>
        <div class="btn-group">
            <button type="button" data-toggle="modal" data-target="#edit_page_modal" class="btn btn-light active"><i class="far fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                <span class="caret"></span>
            </button>
            <div class="dropdown-menu">
                <form action="" method="POST">
                    <input type="hidden" name="page_ID" value="<?= $page->id ?>">
                    <input type="hidden" name="page_author" value="<?= $page->author ?>">
                    <input type="hidden" name="redirect" value="1">
                    <button type="submit" name="remove_page" class="btn btn-link text-danger dropdown-item" <?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? 'disabled' : null ?> onClick="return confirm('Delete this page?')">Delete Page</button>
                </form>
            </div>
        </div>
    </div>

    <div class="mt-4">
        <div class="card">
            <div class="card-header bg-dark text-white p-1">
                <h6 class="text-center m-0"><?= $page->name ?></h6>
                <button id="toggle_editor_fullscreen" class="btn btn-sm pr-2 text-muted">
                    Fullscreen
                    <i class="fas fa-compress"></i>
                </button>
            </div>
            <div id="current_pageEditor" class="editor"></div>
            <xmp id="editorVal" name="editor_value" class="d-none"><?= $page->content ?></xmp>
            <button id="update_page" onclick="updateEditor('<?= $page->link ?>')" class="btn btn-dark border-top-radius-0">Update</button>
        </div>
    </div>

</div>