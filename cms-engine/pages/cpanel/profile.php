<h5 class="mb-4 text-secondary">
    Your Profile
</h5>

<div class="card mt-4">
    <div class="card-header bg-dark text-light text-center p-1">
        <h6 class="m-0">PROFILE</h6>
    </div>
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-row">
                <div class="form-group col-lg-4">
                    <label>Full Name</label>
                    <input class="form-control" name="user_fullname" placeholder="Your Full Name" value="<?= USER_FULLNAME ?>">
                </div>
                <div class="form-group col-lg-4">
                    <label>Username</label>
                    <input class="form-control" name="user_username" placeholder="Account Username" value="<?= USER_USERNAME ?>" readonly>
                </div>
                <div class="form-group col-lg-4">
                    <label>E-Mail</label>
                    <input type="email" class="form-control" name="user_email" placeholder="Your E-Mail" value="<?= USER_EMAIL ?>">
                </div>
            </div>
            <h5 class="text-muted">Account Type</h5>
            <p><?= IS_ADMIN ? 'admin' : 'user' ?></p>
            <button type="submit" class="btn btn-dark btn-sm" name="edit_user_info_submit">Submit</button>
        </form>
    </div>
</div>
<div class="card mt-4">
    <div class="card-header bg-dark text-light text-center p-1">
        <h6 class="m-0">PASSWORD</h6>
    </div>
    <div class="card-body">
        <form action="" method="POST" oninput='new_password_repeat.setCustomValidity(new_password_repeat.value !== new_password.value ? "Passwords do not match." : "")'>
            <div class="form-group">
                <label>Old Password</label>
                <input class="form-control" type="password" name="old_password" required>
            </div>
            <div class="form-group">
                <label>New Password</label>
                <input class="form-control" type="password" name="new_password" required>
            </div>
            <div class="form-group">
                <label>Repeat New Password</label>
                <input class="form-control" type="password" name="new_password_repeat" required>
            </div>
            <button class="btn btn-dark btn-sm" type="submit" name="change_password_submit">Change Password</button>
        </form>
    </div>
</div>