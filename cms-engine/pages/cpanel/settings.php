<?php
$default_page_result = $conn->query("SELECT `id`, `name` FROM `pages` WHERE `status` = 1");
?>
<h5 class="mb-4 text-secondary">
    Website Settings
</h5>
<div class="card">
    <div class="card-header bg-dark text-light text-center p-1">
        <h6 class="m-0">WEBSITE</h6>
    </div>
    <div class="card-body">
        <h5>Favicon</h5>

        <?php if (empty(WEBSITE_FAVICON)) { ?>
            <!-- add profile image -->
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="custom-file mb-2" style="max-width: 400px;">
                    <input type="file" name="favicon" class="custom-file-input" id="customFile" required>
                    <label class="custom-file-label justify-content-start" for="customFile">Choose file</label>
                </div>
                <br>
                <button class="btn btn-dark btn-sm" type="submit" name="add_favicon_submit">Add Icon</button>
            </form>
        <?php } else { ?>
            <img style="max-height:100px" src="../assets/favicon/<?= WEBSITE_FAVICON ?>" alt="user profile image">
            <!-- remove profile image-->
            <form action="" method="POST">
                <input type="hidden" name="favicon" value="<?= WEBSITE_FAVICON ?>">
                <button class="btn btn-danger btn-sm mt-2" type="submit" name="delete_favicon_submit" onClick="return confirm('Remove favicon?')">
                    Delete icon
                </button>
            </form>
        <?php } ?>

        <hr>
        <form action="" method="POST">
            <h5>Identity</h5>
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Title</span>
                </div>
                <input class="form-control" name="site_title" placeholder="Website Title" value="<?= WEBSITE_TITLE ?>" required>
            </div>
            <hr>
            <h5>META Tags</h5>
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Description</span>
                </div>
                <input class="form-control" name="site_description" placeholder="Meta Description" value="<?= WEBSITE_DESCRIPTION ?>">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Author</span>
                </div>
                <input class="form-control" name="site_author" placeholder="Author" value="<?= WEBSITE_AUTHOR ?>">
            </div>
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Keywords</span>
                </div>
                <input class="form-control" name="site_keywords" placeholder="Meta Keywords" value="<?= WEBSITE_KEYWORDS ?>">
            </div>
            <label class="text-muted">Must be separated by <code> , </code></label>
            <hr>
            <h5>SEO | Indexing</h5>
            <div class="form-group">
                <h6 class="text-muted">To be indexed by search engines</h6>
                <label class="switch">
                    <input name="site_robots" type="checkbox" value="<?= WEBSITE_ROBOTS ?>" <?= WEBSITE_ROBOTS === 'index, follow' ? 'checked' : null ?>>
                    <span class="slider round"></span>
                </label>
            </div>
            <hr>
            <h5>Google Analytics</h5>
            <h6 class="text-muted">Example: UA-XXXXXXXXX-X</h6>
            <div class="input-group form-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Tracking Code</span>
                </div>
                <input class="form-control" name="ga_code" placeholder="UA-XXXXXXXXX-X" value="<?= WEBSITE_GA_CODE ?>">
            </div>
            <hr>
            <h5>Components visibility</h5>
            <h6 class="text-muted">Enable / disable <b>Header</b> or <b>Footer</b> entirely</h6>
            <div class="d-flex align-items-center form-group">
                <label class="switch mr-2">
                    <input name="header" type="checkbox" value="<?= WEBSITE_HEADER ?>" <?= WEBSITE_HEADER ? 'checked' : null ?>>
                    <span class="slider round"></span>
                </label>
                <label>Header</label>
            </div>
            <div class="d-flex align-items-center form-group">
                <label class="switch mr-2">
                    <input name="footer" type="checkbox" value="<?= WEBSITE_FOOTER ?>" <?= WEBSITE_FOOTER ? 'checked' : null ?>>
                    <span class="slider round"></span>
                </label>
                <label>Footer</label>
            </div>
            <hr>
            <h5>Maintanance mode</h5>
            <div class="form-group">
                <h6 class="text-muted">Enabling this will make website display Maintenance-Mode</h6>
                <label class="switch">
                    <input name="maintenance_status" type="checkbox" value="<?= WEBSITE_MAINTENANCE ?>" <?= WEBSITE_MAINTENANCE ? 'checked' : null ?>>
                    <span class="slider round"></span>
                </label>
            </div>
            <h5>Bundle all CSS and JS files</h5>
            <div class="form-group">
                <h6 class="text-muted">Enabling this will minify all existing CSS and JS files and bundle them together into 1 single file for each of them</h6>
                <label class="switch">
                    <input name="bundle_all_files" type="checkbox" value="<?= WEBSITE_BUNDLE_ALL_FILES ?>" <?= WEBSITE_BUNDLE_ALL_FILES ? 'checked' : null ?>>
                    <span class="slider round"></span>
                </label>
            </div>
            <hr>
            <h5>Default home page</h5>
            <div class="form-group">
                <h6 class="text-muted">Only the public pages can be set to default.</h6>
                <select class="form-control form-control-sm" style="max-width: 8rem;" name="default_page_ID">
                    <?php
                    if ($default_page_result->num_rows > 0) {
                        while ($default_page = $default_page_result->fetch_object()) {
                    ?>
                            <option value="<?= $default_page->id ?>" <?= WEBSITE_DEFAULT_PAGE_ID == $default_page->id ? 'selected' : null ?>><?= $default_page->name ?></option>
                    <?php
                        }
                    }
                    $default_page_result->close();
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-dark btn-sm" name="identity_submit">Submit</button>
        </form>
    </div>
</div>