<?php
$file_ID = ADMIN_PAGE_PARAM;
$file_result = $conn->query("SELECT * FROM files WHERE id = $file_ID");
$file = $file_result->fetch_object();

$file_path = "assets/$file->type/$file->name.$file->type";

require 'cms-engine/template/components/modals/edit_file.php';
?>
<div id="is_edit_css_js_enabled"></div>
<!-- for monaco editor to get the file ID -->
<input id="file_ID" type="hidden" value="<?= $file->id ?>">

<h5 class="mb-4 text-secondary">
    <a class="text-dark" href="files">CSS & JavaScript Files</a> / <?= $file->name . "." . $file->type ?>
</h5>

<div class="d-flex flex-column">

    <div id="editorSuccess"></div>

    <div class="page-actions d-flex justify-content-between">
        <div class="btn-group">
            <a href="../<?= $file_path ?>" target="_blank" class="btn btn-dark"><i class="fas fa-external-link-alt"></i> View</a>
            <a href="../<?= $file_path ?>" download data-toggle="tooltip" title="Download" class="btn btn-light active"><i class="fas fa-download"></i></a>
        </div>
        <div class="btn-group">
            <button type="button" data-toggle="modal" data-target="#edit_file_modal" class="btn btn-light active"><i class="far fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                <span class="caret"></span>
            </button>
            <div class="dropdown-menu">
                <form action="" method="POST">
                    <input type="hidden" name="file_ID" value="<?= $file->id ?>">
                    <input id="file_name" type="hidden" name="file_name" value="<?= $file->name ?>">
                    <input id="file_type" type="hidden" name="file_type" value="<?= $file->type ?>">
                    <input type="hidden" name="file_author" value="<?= $file->author ?>">
                    <input type="hidden" name="redirect">
                    <button type="submit" name="remove_file" class="btn btn-link text-danger dropdown-item" onClick="return confirm('Delete this file?')">Delete File</button>
                </form>
            </div>
        </div>
    </div>

    <div class="mt-4">
        <div class="card">
            <div class="card-header bg-dark text-white p-1">
                <h6 class="text-center m-0"><?= $file->name . "." . $file->type ?></h6>
                <button id="toggle_editor_fullscreen" class="btn btn-sm pr-2 text-muted">
                    Fullscreen
                    <i class="fas fa-compress"></i>
                </button>
            </div>
            <div id="current_fileEditor" class="editor"></div>
            <xmp id="fileVal" name="file_value" class="d-none"><?php readfile($file_path) ?></xmp>
            <button id="update_file" onclick="updateEditor('<?= $file->name ?>')" class="btn btn-dark border-top-radius-0">Update</button>
        </div>
    </div>

</div>