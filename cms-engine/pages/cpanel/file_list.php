<?php
$css_files_result = $conn->query("SELECT * FROM `files` WHERE `type` = 'css' ORDER BY `position`");
$js_files_result = $conn->query("SELECT * FROM `files` WHERE `type` = 'js' ORDER BY `position`");

require 'cms-engine/template/components/modals/add_css_file.php';
require 'cms-engine/template/components/modals/add_js_file.php';
require 'cms-engine/template/components/modals/remove_file.php';
?>

<h5 class="mb-4 text-secondary">
    CSS & JavaScript Files
</h5>

<div class="d-flex flex-column">
    <div class="row">
        <div class="col-xl-6 col-lg-12 mb-4">
            <button data-toggle="modal" data-target="#add_css_file_modal" class="btn btn-dark btn-sm mb-4" type="button">
                <i class="fas fa-plus"></i>
                Add CSS
            </button>
            <div class="card">
                <div class="card-header bg-dark text-light text-center p-1">
                    <h6 class="m-0">.CSS</h6>
                </div>
                <div class="table-responsive text-nowrap">
                    <table class="table table-hover">
                        <thead class="bg-light">
                            <tr>
                                <th class="pl-3" style="width: 1rem;"></th>
                                <th class="pl-3">Name</th>
                                <th style="width: 6rem;">Author</th>
                                <th style="width: 8rem;">Created</th>
                                <th style="width: 8rem;">Modified</th>
                                <th style="width: 6rem;" class="text-center">Status</th>
                                <th style="width: 6rem;" class="text-center">Minify</th>
                                <th style="width: 6rem;" class="text-center">Remove</th>
                            </tr>
                        </thead>
                        <tbody id="sortable_css_files">
                            <?php
                            if ($css_files_result->num_rows > 0) {
                                while ($css_file = $css_files_result->fetch_object()) {
                            ?>
                                    <tr id="item-<?= $css_file->id ?>" class="<?= $css_file->status ? 'active' : 'disabled' ?>">
                                        <td>
                                            <i class="sort-css-files-handler fas fa-grip-lines cursor-pointer"></i>
                                        </td>
                                        <td class="cursor-pointer" onclick="redirectTo('files/<?= $css_file->id ?>')">
                                            <a data-toggle="tooltip" title="Edit" href="files/<?= $css_file->id ?>"><?= $css_file->name . "." . $css_file->type ?></a>
                                        </td>
                                        <td><?= $css_file->author ?></td>
                                        <td data-toggle="tooltip" title="<?= $css_file->created ?>"><?= format_date($css_file->created) ?></td>
                                        <td data-toggle="tooltip" title="<?= $css_file->modified ?>"><?= format_date($css_file->modified) ?></td>
                                        <td>
                                            <form class="text-center" action="" method="POST">
                                                <input type="hidden" name="file_ID" value="<?= $css_file->id ?>">
                                                <input type="hidden" name="file_name" value="<?= $css_file->name ?>">
                                                <input type="hidden" name="file_type" value="<?= $css_file->type ?>">
                                                <input type="hidden" name="update_file_status">
                                                <label class="switch" data-toggle="tooltip" title="<?= $css_file->status ? 'Disable' : 'Enable' ?>">
                                                    <input name="file_status" onchange="toggleSwitch(this)" type="checkbox" value="<?= $css_file->status ?>" <?= $css_file->status ? 'checked' : null ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </form>
                                        </td>
                                        <td>
                                            <form class="text-center" action="" method="POST">
                                                <input type="hidden" name="file_ID" value="<?= $css_file->id ?>">
                                                <input type="hidden" name="file_name" value="<?= $css_file->name ?>">
                                                <input type="hidden" name="file_type" value="<?= $css_file->type ?>">
                                                <input type="hidden" name="update_file_minify_status">
                                                <label class="switch" data-toggle="tooltip" title="<?= $css_file->minify ? 'Disable minify' : 'Minify' ?>" <?= WEBSITE_BUNDLE_ALL_FILES ? 'disabled' : null ?>>
                                                    <input name="minify" onchange="toggleSwitch(this)" type="checkbox" value="<?= $css_file->minify ?>" <?= $css_file->minify ? 'checked' : null ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </form>
                                        </td>
                                        <td class="text-center">
                                            <button data-toggle="tooltip" title="Remove" class="btn btn-default text-danger p-0" type="submit" name="remove_file_submit" onclick="removeFile('<?= $css_file->id ?>', '<?= $css_file->name ?>', '<?= $css_file->type ?>', '<?= $css_file->author ?>')">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                            <?php }
                            } else {
                                echo "<td class='text-center' colspan='10'> no CSS files </td>";
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-12">
            <button data-toggle="modal" data-target="#add_js_file_modal" class="btn btn-dark btn-sm mb-4" type="button">
                <i class="fas fa-plus"></i>
                Add JS
            </button>
            <div class="card">
                <div class="card-header bg-dark text-light text-center p-1">
                    <h6 class="m-0">.JS</h6>
                </div>
                <div class="table-responsive text-nowrap">
                    <table class="table table-hover">
                        <thead class="bg-light">
                            <tr>
                                <th class="pl-3" style="width: 1rem;"></th>
                                <th class="pl-3">Name</th>
                                <th style="width: 6rem;">Author</th>
                                <th style="width: 8rem;">Created</th>
                                <th style="width: 8rem;">Modified</th>
                                <th style="width: 6rem;" class="text-center">Status</th>
                                <th style="width: 6rem;" class="text-center">Minify</th>
                                <th style="width: 6rem;" class="text-center">Remove</th>
                            </tr>
                        </thead>
                        <tbody id="sortable_js_files">
                            <?php
                            if ($js_files_result->num_rows > 0) {
                                while ($js_file = $js_files_result->fetch_object()) {
                            ?>
                                    <tr id="item-<?= $js_file->id ?>" class="<?= $js_file->status ? 'active' : 'disabled' ?>">
                                        <td>
                                            <i class="sort-js-files-handler fas fa-grip-lines cursor-pointer"></i>
                                        </td>
                                        <td class="cursor-pointer" onclick="redirectTo('files/<?= $js_file->id ?>')">
                                            <a data-toggle="tooltip" title="Edit" href="files/<?= $js_file->id ?>"><?= $js_file->name . "." . $js_file->type ?></a>
                                        </td>
                                        <td><?= $js_file->author ?></td>
                                        <td data-toggle="tooltip" title="<?= $js_file->created ?>"><?= format_date($js_file->created) ?></td>
                                        <td data-toggle="tooltip" title="<?= $js_file->modified ?>"><?= format_date($js_file->modified) ?></td>
                                        <td>
                                            <form class="text-center" action="" method="POST">
                                                <input type="hidden" name="file_ID" value="<?= $js_file->id ?>">
                                                <input type="hidden" name="file_name" value="<?= $js_file->name ?>">
                                                <input type="hidden" name="file_type" value="<?= $js_file->type ?>">
                                                <input type="hidden" name="update_file_status">
                                                <label class="switch" data-toggle="tooltip" title="<?= $js_file->status ? 'Disable' : 'Enable' ?>">
                                                    <input name="file_status" onchange="toggleSwitch(this)" type="checkbox" value="<?= $js_file->status ?>" <?= $js_file->status ? 'checked' : null ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </form>
                                        </td>
                                        <td>
                                            <form class="text-center" action="" method="POST">
                                                <input type="hidden" name="file_ID" value="<?= $js_file->id ?>">
                                                <input type="hidden" name="file_name" value="<?= $js_file->name ?>">
                                                <input type="hidden" name="file_type" value="<?= $js_file->type ?>">
                                                <input type="hidden" name="update_file_minify_status">
                                                <label class="switch" data-toggle="tooltip" title="<?= $js_file->minify ? 'Disable minify' : 'Minify' ?>" <?= WEBSITE_BUNDLE_ALL_FILES ? 'disabled' : null ?>>
                                                    <input name="minify" onchange="toggleSwitch(this)" type="checkbox" value="<?= $js_file->minify ?>" <?= $js_file->minify ? 'checked' : null ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </form>
                                        </td>
                                        <td class="text-center">
                                            <button data-toggle="tooltip" title="Remove" class="btn btn-default text-danger p-0" type="submit" name="remove_file_submit" onclick="removeFile('<?= $js_file->id ?>', '<?= $js_file->name ?>', '<?= $js_file->type ?>', '<?= $js_file->author ?>')">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                            <?php }
                            } else {
                                echo "<td class='text-center' colspan='10'> no JS files </td>";
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>