<?php
$users_query = $conn->query("SELECT * FROM `users` ORDER BY `fullname`");
$users_count = 1;

require 'cms-engine/template/components/modals/add_user.php';
?>

<h5 class="mb-4 text-secondary">
    User Settings
</h5>

<button data-toggle="modal" data-target="#add_user_modal" class="btn btn-dark btn-sm" type="button" name="add_page_submit">
    <i class="fas fa-plus"></i>
    Add User
</button>

<div class="card mt-4">
    <div class="card-header bg-dark text-light text-center p-1">
        <h6 class="m-0">USERS LIST</h6>
    </div>
    <div class="table-responsive">
        <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Full Name</th>
                    <th>Username</th>
                    <th>E-mail</th>
                    <th>Account Type</th>
                    <th style="width: 14em">Last Activity</th>
                    <th style="width: 10px"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($users_query->num_rows > 0) {
                    while ($users_result = $users_query->fetch_object()) {
                        $users_result->type > 1 ? $users_result->type = 'admin' : $users_result->type = 'user';
                ?>
                        <tr>
                            <td><?= $users_count++ ?></td>
                            <td><?= $users_result->fullname ?></td>
                            <td><?= $users_result->username ?></td>
                            <td><?= $users_result->email ?></td>
                            <td><?= $users_result->type ?></td>
                            <td><?= $users_result->last_activity ?></td>
                            <td class="text-center">
                                <?php (IS_ADMIN && USER_ID == $users_result->id) ? $d_none = "d-none" : $d_none = null ?>
                                <div class="dropdown <?= $d_none ?>">
                                    <button type="button" id="edit_user_list" class="btn btn-default p-0 dropdown-toggle" data-toggle="dropdown">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="users/<?= $users_result->id ?>">Edit</a>
                                        <form action="" method="POST">
                                            <input type="hidden" name="user_ID" value="<?= $users_result->id ?>">
                                            <button onClick="return confirm('Remove user <?= $users_result->username ?>?')" type="submit" name="remove_user_submit" class="<?= $d_none ?> btn dropdown-item text-danger">Remove</button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                <?php }
                } ?>
            </tbody>
        </table>
    </div>
</div>