<?php
$css_libraries_result = $conn->query("SELECT * FROM `libraries` WHERE `type` = 'css' ORDER BY `position`");
$js_libraries_result = $conn->query("SELECT * FROM `libraries` WHERE `type` = 'js' ORDER BY `position`");

require 'cms-engine/template/components/modals/add_css_library.php';
require 'cms-engine/template/components/modals/add_js_library.php';
require 'cms-engine/template/components/modals/remove_library.php';
?>

<h5 class="mb-4 text-secondary">
    CDN Libraries (CSS & JavaScript )
</h5>



<div class="row">
    <div class="col-xl-6 col-lg-12 mb-4">
        <button data-toggle="modal" data-target="#add_css_library_modal" class="btn btn-dark btn-sm mb-4" type="button">
            <i class="fas fa-plus"></i>
            Add CSS
        </button>
        <div class="card">
            <div class="card-header bg-dark text-light text-center p-1">
                <h6 class="m-0">CSS</h6>
            </div>
            <div class="table-responsive text-nowrap">
                <table class="table table-hover">
                    <thead class="bg-light">
                        <tr>
                            <th class="pl-3" style="width: 1rem;"></th>
                            <th class="pl-3">Name</th>
                            <th style="width: 6rem;" class="">URL</th>
                            <th style="width: 6rem;" class="text-center">Status</th>
                            <th style="width: 6rem;" class="text-center">Remove</th>
                        </tr>
                    </thead>
                    <tbody id="sortable_css_libraries">
                        <?php
                        if ($css_libraries_result->num_rows > 0) {
                            while ($css_library = $css_libraries_result->fetch_object()) {
                        ?>
                                <tr id="item-<?= $css_library->id ?>" class="<?= $css_library->status ? 'active' : 'disabled' ?>">
                                    <td>
                                        <i class="sort-css-libraries-handler fas fa-grip-lines cursor-pointer"></i>
                                    </td>
                                    <td class="cursor-pointer" onclick="redirectTo('libraries/<?= $css_library->id ?>')">
                                        <a href="libraries/<?= $css_library->id ?>" data-toggle="tooltip" title="Edit">
                                            <?= $css_library->name ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="tooltip" title="<?= $css_library->url ?>" href="<?= $css_library->url ?>" target="_blank">LINK</a>
                                    </td>
                                    <td>
                                        <form class="text-center" action="" method="POST">
                                            <input type="hidden" name="library_ID" value="<?= $css_library->id ?>">
                                            <input type="hidden" name="library_name" value="<?= $css_library->name ?>">
                                            <input type="hidden" name="update_library_status">
                                            <label class="switch" data-toggle="tooltip" title="<?= $css_library->status ? 'Disable' : 'Enable' ?>">
                                                <input name="library_status" onchange="toggleSwitch(this)" type="checkbox" value="<?= $css_library->status ?>" <?= $css_library->status ? 'checked' : null ?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </form>
                                    </td>
                                    <td class="text-center">
                                        <button data-toggle="tooltip" title="Remove" class="btn btn-default text-danger p-0" type="submit" name="remove_library_submit" onclick="removeLibrary('<?= $css_library->id ?>', '<?= $css_library->name ?>')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                        <?php }
                        } else {
                            echo "<td class='text-center' colspan='10'> no CSS libraries </td>";
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div class="col-xl-6 col-lg-12">
        <button data-toggle="modal" data-target="#add_js_library_modal" class="btn btn-dark btn-sm mb-4" type="button">
            <i class="fas fa-plus"></i>
            Add JS
        </button>
        <div class="card">
            <div class="card-header bg-dark text-light text-center p-1">
                <h6 class="m-0">JS</h6>
            </div>
            <div class="table-responsive text-nowrap">
                <table class="table table-hover">
                    <thead class="bg-light">
                        <tr>
                            <th class="pl-3" style="width: 1rem;"></th>
                            <th class="pl-3">Name</th>
                            <th style="width: 6rem;" class="">URL</th>
                            <th style="width: 6rem;" class="text-center">Status</th>
                            <th style="width: 6rem;" class="text-center">Remove</th>
                        </tr>
                    </thead>
                    <tbody id="sortable_js_libraries">
                        <?php
                        if ($js_libraries_result->num_rows > 0) {
                            while ($js_library = $js_libraries_result->fetch_object()) {
                        ?>
                                <tr id="item-<?= $js_library->id ?>" class="<?= $js_library->status ? 'active' : 'disabled' ?>">
                                    <td>
                                        <i class="sort-js-libraries-handler fas fa-grip-lines cursor-pointer"></i>
                                    </td>
                                    <td class="cursor-pointer" onclick="redirectTo('libraries/<?= $js_library->id ?>')">
                                        <a href="libraries/<?= $js_library->id ?>" data-toggle="tooltip" title="Edit">
                                            <?= $js_library->name ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="tooltip" title="<?= $js_library->url ?>" href="<?= $js_library->url ?>" target="_blank">LINK</a>
                                    </td>
                                    <td>
                                        <form class="text-center" action="" method="POST">
                                            <input type="hidden" name="library_ID" value="<?= $js_library->id ?>">
                                            <input type="hidden" name="library_name" value="<?= $js_library->name ?>">
                                            <input type="hidden" name="update_library_status">
                                            <label class="switch" data-toggle="tooltip" title="<?= $js_library->status ? 'Disable' : 'Enable' ?>">
                                                <input name="library_status" onchange="toggleSwitch(this)" type="checkbox" value="<?= $js_library->status ?>" <?= $js_library->status ? 'checked' : null ?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </form>
                                    </td>
                                    <td class="text-center">
                                        <button data-toggle="tooltip" title="Remove" class="btn btn-default text-danger p-0" type="submit" name="remove_library_submit" onclick="removeLibrary('<?= $js_library->id ?>', '<?= $js_library->name ?>')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                        <?php }
                        } else {
                            echo "<td class='text-center' colspan='10'> no JS libraries </td>";
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>