<?php
$pages_result = $conn->query("SELECT * FROM `pages`");

require 'cms-engine/template/components/modals/add_page.php';
require 'cms-engine/template/components/modals/remove_page.php';
?>

<h5 class="mb-4 text-secondary">Pages</h5>

<button data-toggle="modal" data-target="#add_page_modal" class="btn btn-dark btn-sm" type="button" name="add_page_submit">
    <i class="fas fa-plus"></i>
    Add Page
</button>


<div class="row mt-4">
    <div class="col-xl-9">
        <div class="card mb-4">
            <div class="card-header bg-dark text-light text-center p-1">
                <h6 class="m-0">Public Pages</h6>
            </div>
            <div class="table-responsive text-nowrap">
                <table class="table table-hover">
                    <thead class="bg-light">
                        <tr>
                            <th class="pl-3">Title</th>
                            <th style="width: 1em;">URL</th>
                            <th style="width: 6rem;">Author</th>
                            <th style="width: 8rem;">Created</th>
                            <th style="width: 8rem;">Modified</th>
                            <th style="width: 6rem;" class="text-center">Status</th>
                            <th style="width: 6rem;" class="text-center">Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($pages_result->num_rows > 0) {
                            while ($page = $pages_result->fetch_object()) {
                        ?>
                                <tr class="<?= $page->status ? 'active' : 'disabled' ?>">
                                    <td class="cursor-pointer" onclick="redirectTo('pages/<?= $page->id ?>')">
                                        <a data-toggle="tooltip" title="Edit" href="pages/<?= $page->id ?>"><?= $page->name ?></a>
                                        <?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? "<small class='text-muted small-text'> — <i class='fas fa-home'></i> </small>" : null ?>
                                    </td>
                                    <td>
                                        <a data-toggle="tooltip" title="View" href="<?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? '../' : '../' . $page->link ?>" target="_blank">/<?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? null : $page->link ?></a>
                                    </td>
                                    <td><?= $page->author ?></td>
                                    <td data-toggle="tooltip" title="<?= $page->created ?>"><?= format_date($page->created) ?></td>
                                    <td data-toggle="tooltip" title="<?= $page->modified ?>"><?= format_date($page->modified) ?></td>
                                    <td>
                                        <form class="text-center" action="" method="POST">
                                            <input type="hidden" name="page_ID" value="<?= $page->id ?>">
                                            <input type="hidden" name="page_name" value="<?= $page->name ?>">
                                            <input type="hidden" name="update_page_status">
                                            <label class="switch" data-toggle="tooltip" title="<?= $page->status ? 'Disable' : 'Enable' ?>" <?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? 'disabled' : null ?>>
                                                <input name="page_status" onchange="toggleSwitch(this)" type="checkbox" value="<?= $page->status ?>" <?= $page->status ? 'checked' : null ?>>
                                                <span class="slider round"></span>
                                            </label>
                                        </form>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" data-toggle="tooltip" title="Remove" class="<?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? 'disabled' : null ?> btn btn-default <?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? null : 'text-danger
                                            ' ?> p-0" <?= WEBSITE_DEFAULT_PAGE_URL === $page->link ? 'disabled' : null ?> onclick="removePage('<?= $page->id ?>', '<?= $page->name ?>')">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header bg-dark text-light text-center p-1">
                <h6 class="m-0">Template Pages</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="bg-light">
                        <tr>
                            <th class="pl-3">Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="pl-3 cursor-pointer" onclick="redirectTo('pages/not-found')">
                                <a data-toggle="tooltip" title="Edit" href="pages/not-found">Not Found</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="pl-3 cursor-pointer" onclick="redirectTo('pages/maintenance')">
                                <a data-toggle="tooltip" title="Edit" href="pages/maintenance">Maintenance</a>
                                <?= WEBSITE_MAINTENANCE ? "<small class='text-muted small-text'> — Enabled </small>" : null ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>