<?php
$library_ID = ADMIN_PAGE_PARAM;
$library_result = $conn->query("SELECT * FROM `libraries` WHERE `id` = $library_ID");
$library = $library_result->fetch_object();
?>

<h5 class="mb-4 text-secondary">
    <a class="text-dark" href="libraries">Libraries</a> / <?= $library->name ?>
</h5>

<div class="card">
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-group">
                <label>Name</label>
                <input class="form-control" name="library_name" value="<?= $library->name ?>" required>
            </div>
            <div class="form-group">
                <label>URL</label>
                <input class="form-control" name="library_URL" value="<?= $library->url ?>" required>
            </div>
            <div class="form-group">
                <label>Crossorigin</label>
                <input class="form-control" type="text" name="library_crossorigin" placeholder="Library crossorigin" value="<?= $library->crossorigin ?>">
            </div>
            <div class="form-group">
                <label>Integrity</label>
                <input class="form-control" type="text" name="library_integrity" placeholder="Library integrity" value="<?= $library->integrity ?>">
            </div>
            <div class="form-group mb-4">
                <label>Enabled</label> <br>
                <label class="switch">
                    <input name="library_status" type="checkbox" value="<?= $library->status ?>" <?= $library->status ? 'checked' : null ?>>
                    <span class="slider round"></span>
                </label>
            </div>
            <input type="hidden" name="library_ID" value="<?= $library->id ?>">
            <button type="submit" class="btn btn-dark btn-sm" name="library_edit_submit">Submit</button>
        </form>
    </div>
</div>