function redirectTo(location) {
    document.location.href = location;
}

function toggleSwitch(e) {
    setTimeout(() => {
        e.form.submit();
    }, 150);
}

$(document).ready(function () {
    // remove page modal
    removePage = function (pageId, pageName) {
        const pageModalId = "#remove_page_modal";
        $(pageModalId + " input[name='page_ID']").attr("value", pageId);
        $(pageModalId + " input[name='page_name']").attr("value", pageName);
        $(pageModalId + " .modal-body").html(pageName);
        $(pageModalId).modal({ show: true });
    };

    // remove library modal
    removeLibrary = function (libraryId, libraryName) {
        const libraryModalId = "#remove_library_modal";
        $(libraryModalId + " input[name='library_ID']").attr(
            "value",
            libraryId
        );
        $(libraryModalId + " input[name='library_name']").attr(
            "value",
            libraryName
        );
        $(libraryModalId + " .modal-body").html(libraryName);
        $(libraryModalId).modal({ show: true });
    };

    // remove file modal
    removeFile = function (fileId, fileName, fileType, fileAuthor) {
        const fileModalId = "#remove_file_modal";
        $(fileModalId + " input[name='file_ID']").attr("value", fileId);
        $(fileModalId + " input[name='file_name']").attr("value", fileName);
        $(fileModalId + " input[name='file_type']").attr("value", fileType);
        $(fileModalId + " input[name='file_author']").attr("value", fileAuthor);
        $(fileModalId + " .modal-body").html(`${fileName}.${fileType}`);
        $(fileModalId).modal({ show: true });
    };

    // click copy to clickboard
    $(".copyToClipboard").click(function () {
        var textToCopy = $(this).children("textarea"),
            textArea = document.createElement("textarea");

        $(this)
            .attr("data-original-title", "Copied!")
            .tooltip("show")
            .attr("data-original-title", "Copy");

        textArea.value = textToCopy.val();
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        document.execCommand("copy");
        document.body.removeChild(textArea);
    });

    // custom input file for profile picture
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this)
            .siblings(".custom-file-label")
            .addClass("selected")
            .html(fileName);
    });

    // get current admin page
    var admin_root_page = window.location.pathname.split("cms-admin/");
    admin_root_page =
        admin_root_page.length > 1
            ? admin_root_page[1].split("/").shift()
            : null;

    // add active class to sidebar
    $(function () {
        if (!admin_root_page) {
            $(".sidebar a[href='dashboard']").addClass("active");
        } else {
            $(".sidebar a[href=" + admin_root_page + "]").addClass("active");
        }
    });

    // enable Bootstrap 4 Tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    // dropdown effect
    $(".dropdown").on("show.bs.dropdown", function (e) {
        $(this).find(".dropdown-menu").first().stop(true, true).fadeIn("fast");
    });

    $(".dropdown").on("hide.bs.dropdown", function (e) {
        $(this).find(".dropdown-menu").first().stop(true, true).fadeOut("fast");
    });

    // notifications click to dismiss
    $(".notification_close").click(function (e) {
        e.preventDefault();
        var parent = $(this).parent(".notification");
        parent.fadeOut("slow", function () {
            $(this).remove();
        });
    });

    // notifications auto dismiss 5 sec
    $(function () {
        "use strict";
        $(".notification[data-auto-dismiss]").each(function (index, element) {
            setTimeout(function () {
                $(element).fadeOut("slow", function () {
                    $(this).remove();
                });
            }, 5000);
        });
    });

    // search filter
    $("#search_table").keyup(function () {
        var value = $(this).val().toLowerCase();
        $("#search_tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });

    // sort CSS files
    $("#sortable_css_files").sortable({
        handle: ".sort-css-files-handler",
        update: function () {
            var order = $("#sortable_css_files").sortable("serialize");
            $.ajax({
                type: "POST",
                url: "",
                data: { order_files: "css", order: order },
            });
        },
    });

    // sort JS files
    $("#sortable_js_files").sortable({
        handle: ".sort-js-files-handler",
        update: function () {
            var order = $("#sortable_js_files").sortable("serialize");
            $.ajax({
                type: "POST",
                url: "",
                data: { order_files: "js", order: order },
            });
        },
    });

    // sort CSS files
    $("#sortable_css_libraries").sortable({
        handle: ".sort-css-libraries-handler",
        update: function () {
            var order = $("#sortable_css_libraries").sortable("serialize");
            $.ajax({
                type: "POST",
                url: "",
                data: { order_libraries: "css", order: order },
            });
        },
    });

    // sort CSS files
    $("#sortable_js_libraries").sortable({
        handle: ".sort-js-libraries-handler",
        update: function () {
            var order = $("#sortable_js_libraries").sortable("serialize");
            $.ajax({
                type: "POST",
                url: "",
                data: { order_libraries: "js", order: order },
            });
        },
    });
});
