$(document).ready(function () {
    // monaco editor toggle-ON fullscreen
    $("#toggle_editor_fullscreen").click(function () {
        $(".monaco-editor").parent().addClass("editor-fullscreen");
    });
    // monaco editor toggle-OFF fullscreen
    $(document).keyup(function (e) {
        if (e.key === "Escape" || e.key === "Esc" || e.key === 27) {
            // escape key maps to keycode `27`
            $(".monaco-editor").parent().removeClass("editor-fullscreen");
        }
    });

    // when CTRL+S / CMD+S click -> submit save function
    editorClickSubmit = function (editor_submit_button_ID) {
        jQuery(document).keydown(function (event) {
            if ((event.ctrlKey || event.metaKey) && event.which == 83) {
                $("#" + editor_submit_button_ID).click();
                event.preventDefault();
                return false;
            }
        });
    };

    var notification_success =
        "<div class='notification_body fadeInDown animated'><div class='notification notification-success'><div class='notification_content'><p class='notification_type'>Success</p><p class='notification_message'>Code updated!</p></div><div class='notification_close'></div></div></div>";

    // function to create Monaco Editor instances
    createMonacoEditor = function (
        textarea_ID,
        editor_name,
        editor_div_ID,
        submit_post_name,
        monaco_language
    ) {
        var textarea_val = $("#" + textarea_ID).html();

        require.config({
            paths: {
                vs: "../cms-engine/template/assets/ext/monaco-editor/min/vs",
            },
        });
        require(["vs/editor/editor.main"], function () {
            if (document.getElementById("" + editor_div_ID + "")) {
                editor_name = monaco.editor.create(
                    document.getElementById("" + editor_div_ID + ""),
                    {
                        value: textarea_val,
                        language: monaco_language,
                        theme: "vs-dark",
                        wordWrap: "on",
                        automaticLayout: true,
                    }
                );
            }

            // Update Editor Value
            updateEditor = function (page_link) {
                $("#editorSuccess").empty().html(notification_success);

                $(function () {
                    setTimeout(function () {
                        $("#editorSuccess").empty();
                    }, 3000);
                });

                var editor_val = editor_name.getValue(),
                    editor_post = {
                        page_link: page_link,
                        editor_value: editor_val,
                        [submit_post_name]: true,
                    };

                $.ajax({
                    type: "POST",
                    data: editor_post,
                    dataType: "JSON",
                });
            }; // end update editor
        });
    }; // END monaco editor instance

    // specific page editor
    if (document.getElementById("is_edit_page_enabled")) {
        createMonacoEditor(
            "editorVal",
            "editor_specific",
            "current_pageEditor",
            "editor_update_submit",
            "html"
        );
        editorClickSubmit("update_page");
    }

    // Header edit
    if (document.getElementById("is_edit_header_enabled")) {
        createMonacoEditor(
            "navbarVal",
            "editor_nav",
            "navbarEditor",
            "template_update_header_submit",
            "html"
        );
        editorClickSubmit("update_header");
    }

    // Footer edit
    if (document.getElementById("is_edit_footer_enabled")) {
        createMonacoEditor(
            "footerVal",
            "editor_footer",
            "footerEditor",
            "template_update_footer_submit",
            "html"
        );
        editorClickSubmit("update_footer");
    }

    // Not found page edit
    if (document.getElementById("is_edit_404_enabled")) {
        createMonacoEditor(
            "404Val",
            "editor_404",
            "404Editor",
            "template_update_not_found_submit",
            "html"
        );
        editorClickSubmit("update_404");
    }

    // Maintenance page edit
    if (document.getElementById("is_edit_maintenance_enabled")) {
        createMonacoEditor(
            "maintenanceVal",
            "editor_maintenance",
            "maintenanceEditor",
            "template_update_maintenance_submit",
            "html"
        );
        editorClickSubmit("update_maintenance");
    }

    // START CSS / JS file update
    if (document.getElementById("is_edit_css_js_enabled")) {
        var fileVal = $("#fileVal").html(),
            file_ID = $("#file_ID").val(),
            file_name = $("#file_name").val(),
            file_type = $("#file_type").val();

        if (file_type === "js") {
            editor_language = "javascript";
        } else {
            editor_language = "css";
        }

        // style editor
        require.config({
            paths: {
                vs: "../cms-engine/template/assets/ext/monaco-editor/min/vs",
            },
        });
        require(["vs/editor/editor.main"], function () {
            if (document.getElementById("current_fileEditor")) {
                editor = monaco.editor.create(
                    document.getElementById("current_fileEditor"),
                    {
                        value: fileVal,
                        language: editor_language,
                        theme: "vs-dark",
                        wordWrap: "on",
                        automaticLayout: true,
                    }
                );
            }

            // Update Editor Value
            updateEditor = function () {
                $("#editorSuccess").empty().html(notification_success);

                $(function () {
                    setTimeout(function () {
                        $("#editorSuccess").empty();
                    }, 3000);
                });

                var editorVal = editor.getValue(),
                    editorPost = {
                        file_ID: file_ID,
                        file_name: file_name,
                        file_type: file_type,
                        editor_value: editorVal,
                        file_update_submit: true,
                    };

                $.ajax({
                    type: "POST",
                    data: editorPost,
                    dataType: "JSON",
                });
            }; // end update editor
        });
        editorClickSubmit("update_file");
    }
    // END CSS / JS file update
});
