<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">

    <?= get_title() ?>

    <?= get_base_href() ?>

    <!-- Bootstrap 4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Animate.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <link rel="icon" href="<?= WEBSITE_FAVICON ? '../assets/favicon/' . WEBSITE_FAVICON : '#' ?>">

    <?php if (LOGGED_IN) { ?>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../cms-engine/template/assets/css/template_style.css">
    <?php } else { ?>
        <!-- Login CSS -->
        <link rel="stylesheet" href="../cms-engine/template/assets/css/template_login.css">
    <?php } ?>
</head>

<body>

    <?= notification_handler() ?>