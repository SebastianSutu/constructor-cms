<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?= get_client_title() ?></title>

    <base href="<?= get_client_base_href() ?>">

    <meta name="description" content="<?= get_client_description() ?>">
    <meta name="author" content="<?= get_client_author() ?>">
    <meta name="keywords" content="<?= get_client_keywords() ?>">
    <meta name="robots" content="<?= get_client_robots() ?>">

    <meta property="og:type" content="website">
    <meta property="og:title" content="<?= get_client_title() ?>">
    <meta property="og:description" content="<?= get_client_description() ?>">
    <meta property="og:url" content="<?= $_SERVER['SERVER_NAME'] ?>">
    <meta property="og:site_name" content="<?= ucfirst(preg_replace('/.com/', "", $_SERVER['SERVER_NAME'])) ?>">
    <meta property="og:image" content="<?= isset($_SERVER['HTTPS']) ? "https" : "http" ?>://<?= $_SERVER['SERVER_NAME'] ?>/<?= get_client_icon() ?>">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@<?= $_SERVER['SERVER_NAME'] ?>">
    <meta name="twitter:title" content="<?= get_client_title() ?>">
    <meta name="twitter:description" content="<?= get_client_description() ?>">
    <meta name="twitter:image" content="<?= isset($_SERVER['HTTPS']) ? "https" : "http" ?>://<?= $_SERVER['SERVER_NAME'] ?>/<?= get_client_icon() ?>">

    <link rel="icon" href="<?= get_client_icon() ?>">

    <?= get_client_css_libraries() ?>

    <?= get_client_css_files() ?>

    <?= get_client_ga_code() ?>

</head>

<body>

    <?= get_client_header() ?>