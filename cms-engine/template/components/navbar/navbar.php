<!-- Navbar -->
<nav class="navbar top-navbar navbar-expand fixed-top navbar-light bg-light">
    <a class="website-title" href="./">
        <?= WEBSITE_TITLE ?>
    </a>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-alt"></i>
                <span class="logged-user"><?= USER_FULLNAME ?><span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="profile">
                    <i class="fas fa-wrench"></i>
                    Profile
                </a>
                <div class="dropdown-divider"></div>
                <form action="" method="POST">
                    <button type="submit" name="logout" class="dropdown-item">
                        <i class="fas fa-sign-out-alt"></i>
                        Log out
                    </button>
                </form>
            </div>
        </li>
    </ul>
</nav>