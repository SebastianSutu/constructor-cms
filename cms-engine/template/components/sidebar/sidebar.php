<!-- Sidebar -->
<nav class="sidebar">
    <div class="d-flex flex-column">
        <a class="nav-link" title="View Website" target="_blank" href="../">
            <i class="fas fa-eye"></i>
            <span>View Website</span>
        </a>
        <div class="nav-link-divider"></div>
        <a class="nav-link" title="Dashboard" href="dashboard">
            <i class="fas fa-home"></i>
            <span>Dashboard</span>
        </a>
        <a class="nav-link" title="Statistics" href="statistics">
            <i class="far fa-chart-bar"></i>
            <span>Statistics</span>
        </a>
        <div class="nav-link-divider"></div>
        <a class="nav-link" title="Pages" href="pages">
            <i class="fas fa fa-copy"></i>
            <span>Pages</span>
        </a>
        <a class="nav-link" title="CSS & JS" href="files">
            <i class="fas fa-file-code"></i>
            <span>CSS & JS</span>
        </a>
        <a class="nav-link" title="Media files" href="media-files">
            <i class="fas fa-paperclip"></i>
            <span>Media Files</span>
        </a>
        <a class="nav-link" title="Header" href="header">
            <i class="fas fa-window-maximize"></i>
            <span>Header</span>
        </a>
        <a class="nav-link" title="Footer" href="footer">
            <i class="fas fa-window-maximize fa-flip-vertical"></i>
            <span>Footer</span>
        </a>
        <a class="nav-link" title="CSS & JS Libraries" href="libraries">
            <i class="fas fa-link"></i>
            <span>Libraries</span>
        </a>
        <a class="nav-link" title="Shortcodes" href="shortcodes">
            <i class="fas fa-code"></i>
            <span>Shortcodes</span>
        </a>
        <div class="nav-link-divider"></div>
        <a class="nav-link" title="Website" href="settings">
            <i class="fas fa-laptop-code"></i>
            <span>Settings</span>
        </a>
        <?php if (IS_ADMIN) : ?>
            <a class="nav-link" title="Users" href="users">
                <i class="fas fa-users-cog"></i>
                <span>Users</span>
            </a>
        <?php endif ?>
        <a class="nav-link" title="Profile" href="profile">
            <i class="far fa-user-circle"></i>
            <span>Profile</span>
        </a>
    </div>
</nav>