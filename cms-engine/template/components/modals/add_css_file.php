<!-- Add CSS File Modal -->
<div class="modal fade" tabindex="-1" id="add_css_file_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Add New CSS File</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>File name</label>
                        <div class="input-group mb-3">
                            <input class="form-control" type="text" name="file_name" placeholder="File Name" required>
                            <div class="input-group-append">
                                <span class="input-group-text">.css</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <label>Include in website</label><br>
                        <label class="switch">
                            <input name="file_status" type="checkbox" value="1" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>

                <div class="modal-footer ">
                    <input type="hidden" name="file_type" value="css">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-dark" type="submit" name="add_file_submit">Add File</button>
                </div>
            </form>
        </div>
    </div>
</div>