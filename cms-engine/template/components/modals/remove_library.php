<!-- Remove Library Modal -->
<div class="modal fade" tabindex="-1" id="remove_library_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Remove library</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body text-muted"></div>
                <div class="modal-footer">
                    <input type="hidden" name="library_ID">
                    <input type="hidden" name="library_name">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit" name="remove_library">Remove</button>
                </div>
            </form>
        </div>
    </div>
</div>