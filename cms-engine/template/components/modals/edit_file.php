<!-- Edit File Modal -->
<div class="modal fade" tabindex="-1" id="edit_file_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Edit File Information</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="file_ID" value="<?= $file->id ?>">
                    <input type="hidden" name="old_file_name" value="<?= $file->name ?>">
                    <input type="hidden" name="old_minify" value="<?= $file->minify ?>">
                    <input type="hidden" name="file_type" value="<?= $file->type ?>">
                    <input type="hidden" name="file_author" value="<?= $file->author ?>">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" type="text" name="file_name" value="<?= $file->name ?>">
                    </div>
                    <div class="form-group">
                        <label>Minify</label><br>
                        <label class="switch" <?= WEBSITE_BUNDLE_ALL_FILES ? 'disabled' : null ?>>
                            <input name="minify" type="checkbox" value="<?= $file->minify ?>" <?= $file->minify ? 'checked' : null ?>>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="edit_file_submit" class="btn btn-dark">Edit File</button>
                </div>
            </form>
        </div>
    </div>
</div>