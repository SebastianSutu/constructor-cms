<!-- Add Library Modal -->
<div class="modal fade" tabindex="-1" id="add_css_library_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Add New CSS Library</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" type="text" name="library_name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input class="form-control" type="text" name="library_URL" placeholder="CDN URL" required>
                    </div>
                    <div class="form-group">
                        <label>Crossorigin</label>
                        <input class="form-control" type="text" name="library_crossorigin" placeholder="Library crossorigin">
                    </div>
                    <div class="form-group">
                        <label>Integrity</label>
                        <input class="form-control" type="text" name="library_integrity" placeholder="Library integrity">
                    </div>
                    <div class="form-group mb-0">
                        <label>Enabled</label> <br>
                        <label class="switch">
                            <input name="library_status" type="checkbox" value="1" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="library_type" value="css">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-dark" type="submit" name="add_library">Add Library</button>
                </div>
            </form>
        </div>
    </div>
</div>