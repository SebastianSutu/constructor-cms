<!-- Add User Modal -->
<div class="modal fade" tabindex="-1" id="add_user_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST" autocomplete="false" oninput='new_user_password_repeat.setCustomValidity(new_user_password_repeat.value !== new_user_password.value ? "Passwords do not match." : "")'>
                <div class="modal-header">
                    <h5 class="modal-title">Add New User</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Full Name</label>
                        <input class="form-control" type="text" name="new_user_fullname" required>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input class="form-control" type="text" name="new_user_username" required>
                    </div>
                    <div class="form-group">
                        <label>E-Mail (Optional)</label>
                        <input class="form-control" type="email" name="new_user_email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" type="password" name="new_user_password" required>
                    </div>
                    <div class="form-group">
                        <label>Password (Repeat)</label>
                        <input class="form-control" type="password" name="new_user_password_repeat" required>
                    </div>
                    <div class="form-group">
                        <label>Administrator</label> <br>
                        <label class="switch">
                            <input name="new_user_type" type="checkbox" value="2">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>

                <div class="modal-footer ">
                    <button type="button" class="btn  btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn  btn-dark" type="submit" name="add_user_submit">Add User</button>
                </div>
            </form>
        </div>
    </div>
</div>