<!-- Edit Page Modal -->
<div class="modal fade" tabindex="-1" id="edit_page_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Page Information</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="page_ID" value="<?= $page->id ?>">
                    <input type="hidden" name="old_page_title" value="<?= $page->name ?>">
                    <input type="hidden" name="old_page_url" value="<?= $page->link ?>">
                    <input type="hidden" name="page_author" value="<?= $page->author ?>">
                    <input type="hidden" name="old_header" value="<?= $page->header ?>">
                    <input type="hidden" name="old_footer" value="<?= $page->footer ?>">
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" type="text" name="page_title" value="<?= $page->name ?>">
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">/</span>
                            </div>
                            <input class="form-control" type="text" name="page_url" value="<?= $page->link ?>">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="page_description"><?= $page->description ?></textarea>
                        <small class="text-muted">between 50–160 characters</small>
                    </div>
                    <div class="form-group">
                        <label>Keywords</label>
                        <input class="form-control" type="text" name="page_keywords" value="<?= $page->keywords ?>">
                        <small class="text-muted">Must be separated by <code> , </code> </small>
                    </div>
                    <div class="form-group">
                        <label>Indexed by search engines</label><br>
                        <label class="switch">
                            <input name="page_robots" type="checkbox" value="1" <?= $page->robots === 'index, follow' ? 'checked' : null ?>>
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <hr>
                    <h6>Components visibility</h6>
                    <div class="d-flex align-items-center form-group">
                        <label class="switch mr-2" <?= !WEBSITE_HEADER ? 'disabled' : null ?>>
                            <input name="header" type="checkbox" value="<?= $page->header ?>" <?= $page->header ? 'checked' : null ?>>
                            <span class="slider round"></span>
                        </label>
                        <label>Header <?= !WEBSITE_HEADER ? '<small class="text-muted">- disabled globally</small>' : null ?></label>
                    </div>
                    <div class="d-flex align-items-center form-group">
                        <label class="switch mr-2" <?= !WEBSITE_FOOTER ? 'disabled' : null ?>>
                            <input name="footer" type="checkbox" value="<?= $page->footer ?>" <?= $page->footer ? 'checked' : null ?>>
                            <span class="slider round"></span>
                        </label>
                        <label>Footer <?= !WEBSITE_FOOTER ? '<small class="text-muted">- disabled globally</small>' : null ?></label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="edit_page_submit" class="btn btn-dark">Edit Page</button>
                </div>
            </form>
        </div>
    </div>
</div>