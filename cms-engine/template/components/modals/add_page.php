<!-- Add Page Modal -->
<div class="modal fade" tabindex="-1" id="add_page_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Page</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" type="text" name="page_name" placeholder="Page title" required>
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">/</span>
                            </div>
                            <input class="form-control" type="text" name="page_URL" placeholder="Page URL" required>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <label>Public</label> <br>
                        <label class="switch">
                            <input name="page_status" type="checkbox" value="1" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-dark" type="submit" name="add_page">Add Page</button>
                </div>
            </form>
        </div>
    </div>
</div>