<!-- Remove File Modal -->
<div class="modal fade" tabindex="-1" id="remove_file_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title">Remove File</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body text-muted"></div>
                <div class="modal-footer">
                    <input type="hidden" name="file_ID">
                    <input type="hidden" name="file_name">
                    <input type="hidden" name="file_type">
                    <input type="hidden" name="file_author">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit" name="remove_file">Remove</button>
                </div>
            </form>
        </div>
    </div>
</div>