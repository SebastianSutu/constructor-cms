<?php

if (CPANEL_ROUTE) {
    require 'components/header/admin.php';
    if (LOGGED_IN) {
        require 'components/navbar/navbar.php';
        require 'components/sidebar/sidebar.php';
    }
    echo "<div class='wrapper'>";
} else {
    require 'components/header/client.php';
}
